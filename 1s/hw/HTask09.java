/**
* @author Denis Shegay
* 11-901
* Task 09
*/
import java.util.*;
import java.io.*;

public class HTask09{

	public static void main(String args[]){
	
		Scanner in = new Scanner(System.in);
		double x = in.nextDouble();
		double y = in.nextDouble();
		if (y >= 0)
			if (y <= x + 1 && y <= (-1)*x + 1)
				System.out.print("yes");
			else
				System.out.print("no");
		else
			System.out.print("no");
	}
}