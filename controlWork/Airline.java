package controlWork;

public class Airline {

    private String name, abbreviation, country;

    public Airline(String name, String abbreviation, String country) {

        this.name = name;
        this.abbreviation = abbreviation;
        this.country = country;
    }

    public String getName() {

        return name;
    }

    public String getAbbreviation() {

        return abbreviation;
    }

    public String getCountry() {

        return country;
    }

    public String toString() {

        return name;
    }
}
