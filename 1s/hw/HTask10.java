/**
* @author Denis Shegay
* 11-901
* Task 10
*/
import java.util.*;
import java.io.*;

public class HTask10{

	public static void main(String args[]){
	
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		int y = in.nextInt();
		int k = 0;
		if (y > -1 && y < 5 && x < 3 && x > -5)
			if ((x < 1 && Math.sqrt(x*x + y*y) < 5) || (x > 0 && y <= 4 - 2*x)){
				System.out.print("In");
				k = 1;
			}
		if (k == 0)
			System.out.print("Out");
	}
}