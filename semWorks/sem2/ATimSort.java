package sem2;

import java.util.Stack;

import static java.lang.Math.*;

public class ATimSort {

    private static int iterations;
    private static int[] a;

    public static void sort(int[] arrayToSort) {

        iterations = 0;
        a = arrayToSort;
        int n = a.length;
        int minRun = getMinRun(n);
        boolean decreasing = false;
        boolean complemented;
        int decreaseTo = 0;
        int i = 0;
        Stack<Array> arrays = new Stack<>();
        int arraySize = 1;
        int stackCounter = 0;

        while (i + 1 < n) {

            if (a[i] <= a[i + 1]) {
                arraySize = up(a, i, arraySize);
            } else {
                arraySize = down(a, i, arraySize);
                decreasing = true;
                decreaseTo = i + arraySize - 1;
            }

            int tempI = i;
            int prevSize = arraySize;
            while (arraySize < minRun && tempI + prevSize < n) {
                arraySize++;
                tempI++;
            }

            complemented = tempI > i;

            Array x = new Array(i, arraySize);

            if (decreasing)
                reverse(a, x.getBegin(), decreaseTo);

            if (complemented)
                insertionSort(a, x.getBegin(), x.getEnd());

            if (stackCounter >= 2) {
                Array y = arrays.pop();
                Array z = arrays.pop();
                if (x.getSize() <= y.getSize() + z.getSize() || y.getSize() <= z.getSize()) {
                    Array array = x.getSize() < z.getSize() ? x : z;
                    if (array == x) {
                        arrays.push(z);
                        arrays.push(merge(array, y, a));
                    } else {
                        arrays.push(merge(y, array, a));
                        arrays.push(x);
                    }
                }
            } else {
                arrays.push(x);
                stackCounter++;
            }

            decreaseTo = 0;
            decreasing = false;
            arraySize = 1;
            i = arrays.peek().getEnd() + 1;
        }

        while (arrays.size() >= 2) {
            Array array1 = arrays.pop();
            Array array2 = arrays.pop();
            arrays.push(merge(array1, array2, a));
        }
    }

    public static Array merge(Array a1, Array a2, int[] a) {

        boolean canGallop = false;
        int firstLeft = a1.getBegin();
        int firstRight = a1.getEnd();
        int secondLeft = a2.getBegin();
        int secondRight = a2.getEnd();
        int gallopThreshold = 7;
        int row = 0;
        int[] ans = new int[max(firstRight, secondRight) - min(firstLeft, secondLeft) + 1];
        int maxLeft, maxRight, minLeft, minRight, pos, added, currentDigit, counter;
        boolean maxIsFirst;
        boolean fromFirst = false;
        boolean fromSecond = false;
        int kLeft = 0;
        int kRight = ans.length - 1;

        while ((firstLeft <= firstRight || secondLeft <= secondRight) && kLeft <= kRight) {
            if (canGallop) {
                if (firstRight - firstLeft > secondRight - secondLeft) {
                    maxLeft = firstLeft;
                    maxRight = firstRight;
                    minLeft = secondLeft;
                    minRight = secondRight;
                } else {
                    maxLeft = secondLeft;
                    maxRight = secondRight;
                    minLeft = firstLeft;
                    minRight = firstRight;
                }

                if (minLeft > minRight) {
                    if (maxRight + 1 - maxLeft >= 0)
                        System.arraycopy(a, maxLeft, ans, kLeft + maxLeft - maxLeft, maxRight + 1 - maxLeft);
                    break;
                }

                added = 0;
                counter = 0;
                maxIsFirst = firstLeft == maxLeft;

                //left

                pos = binarySearchLeft(a, a[minLeft], maxLeft, maxRight);
                //все до индекса pos меньше чем a[minleft]

                if (pos != -1) {
                    for (int i = maxLeft; i <= pos; i++) {
                        ans[kLeft - maxLeft + i] = a[i];
                        counter++;
                    }
                    kLeft += counter;
                }
                ans[kLeft] = a[minLeft];
                kLeft++;
                added++;

                if (maxIsFirst) {
                    firstLeft += counter;
                    secondLeft++;
                } else {
                    firstLeft++;
                    secondLeft += counter;
                }

                if (kLeft <= kRight) {
                    counter = 0;
                    pos = binarySearchRight(a, a[minRight], maxLeft, maxRight);

                    if (pos != -1) {
                        for (int i = maxRight; i >= pos; i--) {
                            ans[kRight - maxRight + i] = a[i];
                            counter++;
                        }
                        kRight -= counter;
                    }
                    ans[kRight] = a[minRight];
                    kRight--;
                    added++;

                    if (maxIsFirst) {
                        firstRight -= counter;
                        secondRight--;
                    } else {
                        firstRight--;
                        secondRight -= counter;
                    }
                }

                canGallop = added >= gallopThreshold;
                iterations += added;
                row = 0;
            } else {
                if (firstLeft <= firstRight && secondLeft <= secondRight)
                    currentDigit = min(a[firstLeft], a[secondLeft]);
                else
                    currentDigit = firstLeft > firstRight ? a[secondLeft] : a[firstLeft];

                if (firstLeft <= firstRight && currentDigit == a[firstLeft]) {
                    firstLeft++;
                    ans[kLeft] = currentDigit;
                    kLeft++;
                    iterations++;
                    if (fromFirst)
                        row++;
                    else
                        row = 1;
                    fromFirst = true;
                    fromSecond = false;
                } else {
                    ans[kLeft] = currentDigit;
                    kLeft++;
                    iterations++;
                    secondLeft++;
                    if (fromSecond)
                        row++;
                    else
                        row = 1;
                    fromFirst = false;
                    fromSecond = true;
                }

                canGallop = row == gallopThreshold;
            }
        }

        int begin = min(a1.getBegin(), a2.getBegin());
        System.arraycopy(ans, 0, a, begin, ans.length);
        iterations += ans.length - begin + 1;

        return new Array(begin, ans.length);
    }

    public static void reverse(int[] a, int begin, int end) {

        int n = end - begin + 1;
        int temp;

        for (int i = 0; i < n / 2; i++) {
            temp = a[i + begin];
            iterations++;
            a[i + begin] = a[n + begin - i - 1];
            a[n + begin - i - 1] = temp;
        }

    }

    public static void insertionSort(int[] a, int begin, int end) {

        iterations++;
        for (int i = begin; i <= end; i++) {
            int key = a[i];
            int j = i - 1;

            while (j >= begin && a[j] > key) {
                a[j + 1] = a[j];
                iterations++;
                j = j - 1;
            }

            a[j + 1] = key;
        }
    }

    public static int binarySearchLeft(int[] a, int key, int begin, int end) { //все до a[index] <= key

        int mid;
        int constEnd = end;
        if (a[begin] > key)
            return -1;

        while (begin < end) {
            iterations++;
            mid = (begin + end) / 2;
            if (key > a[mid])
                begin = mid + 1;
            else if (key < a[mid])
                end = mid;
            else {
                while (constEnd >= mid && a[mid + 1] == key)
                    mid++;
                return mid;
            }
        }

        if (key < a[begin])
            return begin - 1;
        else
            return begin;
    }

    public static int binarySearchRight(int[] a, int key, int begin, int end) {

        int mid;
        int constBegin = begin;
        if (a[end] < key)
            return -1;

        while (begin < end) {
            iterations++;
            mid = (begin + end) / 2;
            if (key > a[mid])
                begin = mid + 1;
            else if (key < a[mid])
                end = mid;
            else {
                while (mid > constBegin && a[mid - 1] == key)
                    mid--;
                return mid;
            }
        }

        if (key > a[begin])
            return begin + 1;
        else
            return begin;
    }

    public static int down(int[] a, int index, int arraySize) {

        if (a[index] > a[index + 1]) {
            arraySize++;
            index++;
            if (index < a.length - 1)
                arraySize = down(a, index, arraySize);
        }

        return arraySize;
    }

    public static int up(int[] a, int index, int arraySize) {

        if (a[index] <= a[index + 1]) {
            arraySize++;
            index++;
            if (index + 1 < a.length)
                arraySize = up(a, index, arraySize);
        }

        return arraySize;
    }

    public static int getMinRun(int n) {

        int r = 0;

        while (n >= 64) {
            iterations++;
            r |= n & 1;
            n >>= 1;
        }

        return n + r;
    }

    public static int getIterations() {

        return iterations;
    }

    public static boolean checkSorted() {

        for (int i = 0; i < a.length - 1; i++)
            if (a[i] > a[i + 1])
                return false;

        return true;
    }
}