package sem1;

public class Node {

    private Participant value;
    Node next;

    public Node(Participant value) {

        this.value = value;
    }

    public Node() {

        this.value = null;
    }

    public Participant getValue() {

        return value;
    }
}
