/**
* @author Denis Shegay
* 11-901
* Task14
*/
import java.util.*;
import java.io.*;

public class HTask14{
	
	public static void main(String args[]){
		
		Scanner in = new Scanner(System.in);
		double x = in.nextDouble();
		double y = 0.0;
		if (x > 0 && x <= 2)
			y = (x - 1)*(x + 1)*(x + 2);
		if (x > 2)
			y = (x - 1)*(x + 1)/(x + 2);
		else
			y = x*x*(1 + 2*x);
		System.out.print(y);
	}
}