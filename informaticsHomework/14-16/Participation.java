package filmTask;

public class Participation {

    private int filmId;
    private int actorId;
    private String name;

    public Participation(String name, int filmId, int actorId) {

        this.name = name;
        this.actorId = actorId;
        this.filmId = filmId;
    }

    public String getName() {

        return name;
    }

    public int getActorId() {

        return actorId;
    }

    public int getFilmId() {

        return filmId;
    }

}
