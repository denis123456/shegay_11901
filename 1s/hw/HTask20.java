/**
* @author Denis Shegay
* 11-901
* Task 20
*/

import java.util.*;

public class HTask20 {
	
	public static void main(String args[]) {
		
		Scanner in = new Scanner(System.in);
		String m = in.nextLine();
		int n = Integer.parseInt(m);
		String[] s = new String[n];
		for (int i = 0; i < n; i++)
			s[i] = in.nextLine();
		s = sort(s, n);
		for (int i = 0; i < n; i++)
			System.out.println(s[i]);
	}
	
	public static String[] sort(String[] s, int n) {
		
		String a = "";
		for (int i = 0; i < n; i++) 
			for (int j = i + 1; j < n; j++) 
				if (isFirst(s[i], s[j])) {
					a = s[i];
					s[i] = s[j];
					s[j] = a;
				}
				
		return s;
	}
	
	public static boolean isFirst(String a, String b) {
		
		int i = 0;
		int min = 0;
		if (a.length() < b.length())
			min = a.length();
		else
			min = b.length();
		
		while (i < min) {
			if (a.charAt(i) < b.charAt(i))
				return false;
			if (a.charAt(i) > b.charAt(i))
				return true;
			i++;
		}
		
		if (i == min) 
			if (min == a.length())
				return false;
			else
				return true;
			
		return true;
	}
}