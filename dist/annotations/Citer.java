package annotations;

public class Citer {

    @modify
    @loud
    public String voice() {

        return "voice";
    }

    @modify
    @loud
    @noEnglish
    public String voxophone() {

        return "message сообщение";
    }

    @modify
    @mayakovsky
    public String say() {

        return "word anotherWord thirdWord";
    }

    public String message() {

        return "text";
    }

    public static void main(String[] args) {

        Citer citer = new Citer();
        (new Modificator()).modify(citer);
    }
}
