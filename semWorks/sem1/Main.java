package sem1;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        CircleList list = new CircleList();
        list.show();
        System.out.println();
        Participant p1 = new Participant("Jack", "male");
        Participant p2 = new Participant("Tom", "male");
        Participant p3 = new Participant("Emma", "female");
        list.insert(p1);
        list.insert(p2);
        list.insert(p3);
        list.show();
        System.out.println();
        System.out.println(list.search("Emma"));
        System.out.println();
        //list.delete("Emma");
        //list.show();
        System.out.println();
        CircleList[] list2 = list.gender();
        list2[0].show();
        System.out.println();
        list2[1].show();
        System.out.println();
        list.sort("Emma");
        list.show();
        System.out.println();
        Participant p = list.last(3);
        System.out.println(p);
    }
}
