package practice.feb21;

public class Task5 {

    public static void main(String[] args) {

        String s = "5,5,+";
        String[] a;
        a = s.split(",");
        MyLinkedStack<String> stack = new MyLinkedStack<>();
        MyLinkedStack<Integer> digits = new MyLinkedStack<>();

        for (int i = a.length - 1; i >= 0; i--)
            stack.push(a[i]);

        while (!stack.isEmpty()) {
            switch (stack.peek()) {

                case("+"):
                    operation(stack.peek(), digits);
                    break;

                case("-"):
                    operation(stack.peek(), digits);
                    break;

                case("*"):
                    operation(stack.peek(), digits);
                    break;

                case("/"):
                    operation(stack.peek(), digits);
                    break;

                default:
                    digits.push(Integer.parseInt(stack.peek()));
            }

            stack.pop();
        }

        System.out.println(digits.peek());
    }

    public static void operation
            (String operation,
             MyLinkedStack<Integer> digits) {

        int a = 0;
        int b = 0;

        switch (operation) {

            case ("+"):
                a = digits.peek();
                digits.pop();
                b = digits.peek();
                digits.pop();
                digits.push(a + b);
                break;

            case ("-"):
                a = digits.peek();
                digits.pop();
                b = digits.peek();
                digits.pop();
                digits.push(a - b);
                break;

            case ("*"):
                a = digits.peek();
                digits.pop();
                b = digits.peek();
                digits.pop();
                digits.push(a * b);
                break;

            case ("/"):
                a = digits.peek();
                digits.pop();
                b = digits.peek();
                digits.pop();
                digits.push(a / b);
                break;

        }
    }

}
