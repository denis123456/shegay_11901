package practice.feb21;

public class MyArrayStack<T> implements IMyStack<T>{

    private int size, capacity = 1000;
    private T[] a;

    public MyArrayStack() {

        size = 0;
        a = (T[]) new Object[capacity];
    }

    @Override
    public void push(T element) {

        if (size == capacity)
            enlarge();

        a[size] = element;
        size++;
    }

    @Override
    public void pop() {

        a[size - 1] = null;
        size--;
    }

    @Override
    public T peek() {

        return a[size];
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    private void enlarge() {

        capacity += 10;
        T b[] = (T[]) new Object[capacity];

        for (int i = 0; i < size; i++)
            b[i] = a[i];

        a = b;
    }

}
