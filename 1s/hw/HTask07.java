/**
* @author Denis Shegay
* 11-901
* Task 07
*/
import java.util.*;
import java.io.*;

public class HTask07{
	
	public static void main(String args[]) {
	
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = 0;
		for (int k = 0; k < n; k++){
			for (int i = 1; i < 2*n - k; i++)
				System.out.print(" ");
			for (int i = 0; i < 1 + m; i++)
				System.out.print(1);
			m += 2;
			System.out.println();
		}
		m = 0;
		for (int k = 0; k < n; k++){
			for (int i = 0; i < n - k - 1; i++)
				System.out.print(" ");
			for (int i = 0; i < k + m + 1; i++)
				System.out.print(1);
			for (int i = 0; i < 2*n - 1 - 2*m; i++)
				System.out.print(" ");
			for (int i = 0; i < k + m + 1; i++)
				System.out.print(1);
			m ++;
			System.out.println();
		}
	}
}