package annotations;

import java.lang.reflect.*;
import java.lang.annotation.Annotation;

public class Modificator {

    public void modify(Citer citer) {

        Class<? extends Citer> c = citer.getClass();
        for (Method m : c.getMethods()) {
            Annotation[] annos = m.getAnnotations();
            for (Annotation a : annos) {
                if (a.annotationType() == modify.class) {
                    try {
                        for (Annotation a2 : annos) {
                            if (a2.annotationType() == mayakovsky.class) {
                                String[] words = ((String)m.invoke(citer)).split(" ");
                                for (String word : words)
                                    System.out.println(word);
                            } else if (a2.annotationType() == noEnglish.class) {
                                String word = (String)m.invoke(citer);
                                for (int i = 0; i < word.length(); i++)
                                    if (!(word.charAt(i) <= 'z' && word.charAt(i) >= 'a') && !(word.charAt(i) <='Z' && word.charAt(i) >= 'A'))
                                        System.out.print(word.charAt(i));
                                System.out.println();
                            } else if (a2.annotationType() == loud.class) {
                                System.out.print(((String)m.invoke(citer)).toUpperCase());
                                System.out.println();
                            }
                        }
                        break;
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
