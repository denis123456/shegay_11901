public class HTask44_ComplexMatrix2x2 { 

	private ComplexNumber[][] a = new ComplexNumber[2][2];
	
	public ComplexMatrix2x2() {
		
		this(new ComplexNumber(),
			 new ComplexNumber(),
			 new ComplexNumber(),
			 new ComplexNumber());
	}
	
	public ComplexMatrix2x2(ComplexNumber a) {
		
		this(a, a, a, a);
	}
	
	public ComplexMatrix2x2(ComplexNumber a, ComplexNumber b,
	ComplexNumber c, ComplexNumber d) {
		
		this.a[0][0] = a;
		this.a[0][1] = b;
		this.a[1][0] = c;
		this.a[1][1] = d;
	}
	
	public ComplexMatrix2x2 add(ComplexMatrix2x2 matrix) {
		
		ComplexMatrix2x2 newMatrix = new ComplexMatrix2x2();
		newMatrix.a[0][0] = this.a[0][0].add(matrix.a[0][0]);
		newMatrix.a[0][1] = this.a[0][1].add(matrix.a[0][1]);
		newMatrix.a[1][0] = this.a[1][0].add(matrix.a[1][0]);
		newMatrix.a[1][1] = this.a[1][1].add(matrix.a[1][1]);
		
		return newMatrix;
	}

	public ComplexMatrix2x2 mult(ComplexMatrix2x2 matrix) {
		
		ComplexMatrix2x2 newMatrix = new ComplexMatrix2x2();
		
		newMatrix.a[0][0] = 
		this.a[0][0].mult(matrix.a[0][0]).add(this.a[0][1].mult(matrix.a[1][0]));
		newMatrix.a[0][1] = 
		this.a[0][0].mult(matrix.a[1][0]).add(this.a[0][1].mult(matrix.a[1][1]));
		newMatrix.a[1][0] = 
		this.a[1][0].mult(matrix.a[0][0]).add(this.a[1][1].mult(matrix.a[1][0]));
		newMatrix.a[1][1] =
		this.a[1][0].mult(matrix.a[0][1]).add(this.a[1][1].mult(matrix.a[1][1]));
		
		return newMatrix;
	}

	public ComplexNumber det() {
		
		ComplexNumber number = new ComplexNumber();
		number.setA((this.a[0][0].mult(this.a[1][1]).sub(this.a[0][1].mult(this.a[1][0]))).getA());
		number.setB((this.a[0][0].mult(this.a[1][1]).sub(this.a[0][1].mult(this.a[1][0]))).getB());
		
		return number;
	}
	
	public ComplexVector2D multVector(ComplexVector2D vector) {
		
		ComplexVector2D newVector = new ComplexVector2D();
		
		newVector.setA(this.a[0][0].mult(vector.getA()).sub(this.a[0][1].mult(vector.getA())));
		newVector.setB(this.a[1][0].mult(vector.getB()).sub(this.a[1][1].mult(vector.getB())));
		
		return newVector;
	}
	
	public String toString() {
		
		return "" + this.a[0][0] + " " + this.a[0][1] + "\n" 
				+ this.a[1][0] + " " + this.a[1][1];
	}
}