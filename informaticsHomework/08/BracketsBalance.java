package tasks;

import java.util.Scanner;
import java.util.Stack;

public class BracketsBalance {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        System.out.println(solve(s));
    }

    public static boolean solve(String s) {

        Stack<Character> stack = new Stack<>();
        char current;

        for (int i = 0; i < s.length(); i++) {
            current = s.charAt(i);
            if (current == '(' || current == '{' || current == '[')
                stack.push(current);
            else
            if (!stack.isEmpty() && stack.peek() == current)
                stack.pop();
            else
                return false;
        }

        return true;
    }
}
