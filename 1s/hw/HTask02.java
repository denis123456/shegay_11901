/**
* @author Denis Shegay
* 11-901
* Task 02
*/
import java.util.*;
import java.io.*;

public class HTask02{
	
	public static void main(String args[]){
	
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a = 0;
		int s = 0;
		for (int i = 0; i < n; i++){
			a = in.nextInt();
			s += a;
		}
		System.out.print(s);
	}
}