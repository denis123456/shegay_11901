package tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public class ContactsBook {

    public static void main(String[] args) throws FileNotFoundException {

        Map<String, LinkedList<String>> map = new HashMap<>();
        File file = new File("C:\\Users\\densh\\IdeaProjects\\classwork\\src\\input");
        Scanner in = new Scanner(file);
        String[] s;
        while (in.hasNextLine()) {
            s = in.nextLine().split(" ");
            if (!map.containsKey(s[0]))
                map.put(s[0], new LinkedList<>());
            map.get(s[0]).add(s[1]);
        }
        map.entrySet().forEach(System.out::println);
    }
}
