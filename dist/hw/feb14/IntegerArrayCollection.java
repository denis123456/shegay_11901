import java.util.Collection;
import java.util.Iterator;

public class IntegerArrayCollection implements Collection<Integer> {

    private int[] array;
    private int size;
    private int capacity;

    public IntegerArrayCollection() {

        capacity = 1000;
        array = new int[capacity];
        size = 0;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    @Override
    public boolean contains(Object o) {

        for(int i = 0; i < size; i++)
            if (array[i] == (int)o)
                return true;

        return false;
    }

    @Override
    public Iterator<Integer> iterator() {

        return null;
    }

    @Override
    public Object[] toArray() {


        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {

        return null;
    }

    @Override
    public boolean add(Integer x) {

        if (size == capacity)
            enlarge();
        size++;
        array[size] = x;

        return true;
    }

    public void enlarge() {

        capacity += 10;
        int[] array2 = new int[capacity];
        for (int i = 0; i < size; i++)
            array2[i] = array[i];
        array = new int[capacity];
        array = array2;
    }

    @Override
    public boolean remove(Object o) {

        for (int i = 0; i < size; i++)
            if (array[i] == (int)o)
                shift(i);

        return false;
    }

    public void shift(int i) {

        for (int j = i; j < size; j++)
            array[j] = array[j + 1];
        size--;
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        for (Object x : c)
            if(!contains(x))
                return false;

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {

        return false;
    }

    public int getSize() {

        return size;
    }

    @Override
    public boolean removeAll(Collection<?> c) {

        for (Object x : c)
            remove(x);
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        for (Object x : c)
            if (contains(x))
                remove(x);

        return true;
    }

    @Override
    public void clear() {

        for (int i = 0; i < size; i++)
            array[i] = 0;
        size = 0;
    }

}