package practice.feb21;

interface IMyStack<T> {

    default void push(T a) {}
    default void pop() {}
    default T peek() {return null;}
    default boolean isEmpty() {return false;}

}
