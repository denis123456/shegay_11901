import java.util.*;

public class Main {

	public static void main(String[] args) {
		
		int n = 2;
		Rectangle[] r = new Rectangle[n];
	
		r[0] = new Rectangle(0, 0, 0, 1, 1, 0, 1, 1);
		r[1] = new Rectangle(1, 1, 1, 2, 2, 1, 2, 2);
		
		
		System.out.println(r[0].square());
		System.out.println(r[1].perimeter());
		System.out.println(r[0].crosses(r[1]));
		System.out.println(r[0].equals(r[1]));
		System.out.println(r[0]);
		System.out.println(wholePerimeter(r, n));
		System.out.println(cross(r, n));
	}
	
	public static double wholePerimeter(Rectangle[] r, int n) {
		
		double square = 0;
		
		for	(int i = 0; i < n; i++)
			square += r[i].perimeter();
	
		return square;
	}
	
	public static boolean cross(Rectangle[] r, int n) {
		
		for (int i = 0; i < n - 1; i++)
			if (!(r[i].crosses(r[i + 1])))
				return false;
		
		return true;
	}
}