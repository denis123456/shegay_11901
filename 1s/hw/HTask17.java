/**
* @author Denis Shegay
* 11-901
* Task 17
*/
import java.util.*;
import java.io.*;

public class HTask17 {
	
	public static void main(String args[]) {
	 
		Scanner in = new Scanner(System.in);
		final double C = 1e-5;
		double x0 = in.nextDouble();
		double x = x0;
		double s = 0;
		double a = x0;
		int k = 1;
		int m = 1;
		while (Math.abs(a) >= C) {
			s += a;
			m *= -1;
			x *= x0;
			a = m*x/++k;
		}
		System.out.print(s);
	}
}