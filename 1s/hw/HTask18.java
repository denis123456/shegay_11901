/**
* @author Denis Shegay
* 11-901
* Task 18
*/
import java.util.*;
import java.io.*;

public class HTask18 {
	
	public static void main(String args[]) {
	
		Scanner in = new Scanner(System.in);
		final double C = 1e-5;
		int k = 0;
		int f = 1;
		double x0 = in.nextDouble();
		double x = 1;
		int m = -1;
		double s = 0;
		double a = -1;
		while (Math.abs(a) >= C) {
			s += a;
			m *= -1;
			f *= (k + 1)*(k + 2);
			k += 2;
			x *= x0*x0;
			a = m*x/f;
		}
		System.out.print(s);
	}
}