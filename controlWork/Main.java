package controlWork;

import kotlin.reflect.jvm.internal.impl.types.FlexibleType;
import unsorted.IntegerArrayCollection;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.channels.FileLock;
import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        File airportsFile = new File("airports100.csv");
        File airlinesFile = new File("airlines.csv");
        File flightsFile = new File("flights.csv");
        HashMap<String, Airport> airports = new HashMap<>();
        readAirports(airportsFile, airports);
        HashMap<Integer, Airline> airlines = new HashMap<>();
        readAirlines(airlinesFile, airlines);
        LinkedList<Flight> flights = new LinkedList<>();
        readFlights(flightsFile, flights);
        stask1(flights, airlines);
        stask2(flights);
    }

    public static void stask1(LinkedList<Flight> flights, HashMap<Integer, Airline> airlines) {

        Map<Integer, Long> map = flights
                .stream()
                .collect(Collectors.groupingBy(Flight::getAirline, Collectors.counting()));

        Integer k = map.entrySet()
                .stream()
                .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
                .get()
                .getKey();

        System.out.println(airlines.get(k));
    }

    public static void stask2(LinkedList<Flight> flights) {

        Map<String, Long> out = flights.stream()
                .collect(Collectors.groupingBy(Flight::getSource, Collectors.counting()));

        Map<String, Long> in = flights.stream()
                .collect(Collectors.groupingBy(Flight::getDestination, Collectors.counting()));

        in.entrySet()
                .stream()
                .map(x -> (x.getValue() + out.get(x.getKey())))
                .collect(Collectors.toList())
                .stream()
                .forEach(System.out::println);
    }

    public static void readAirports(File file, HashMap<String, Airport> map) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String s = in.nextLine();
        String data[];
        while (in.hasNextLine()) {
            s = in.nextLine();
            data = s.split(",");
            map.put(data[2], new Airport(data[0], data[1], data[3], data[4]));
        }
    }

    public static void readAirlines(File file, HashMap<Integer, Airline> map) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String s = in.nextLine();
        String data[];
        s = in.nextLine();
        while (!s.equals("")) {
            data = s.split(",");
            map.put(Integer.parseInt(data[0]), new Airline(data[1], data[2], data[3]));
            s = in.nextLine();
        }
    }

    public static void readFlights(File file, LinkedList<Flight> list) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String s = in.nextLine();
        String number;
        String source;
        String dest;
        String[] data;
        while (in.hasNextLine()) {
            number = "";
            source = "";
            dest = "";
            s = in.nextLine();
            data = s.split(",");
            for (int i = 1; i < data[1].length(); i++)
                number += "" + data[1].charAt(i);
            for (int i = 2; i < data[2].length(); i++) {
                source += data[2].charAt(i);
                dest += data[3].charAt(i);
            }
            list.add(new Flight(Integer.parseInt(data[0]), Integer.parseInt(number), source, dest));
        }
    }
}

