package myCollections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyArrayList implements List {

    private int[] a;
    private int size;
    private int capacity;

    public MyArrayList() {

        capacity = 10;
        a = new int[capacity];
        size = 0;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    @Override
    public boolean contains(Object o) {

        for (Integer x : a)
            if (x == o)
                return true;

        return false;
    }

    @Override
    public Iterator iterator() {

        return new MyArrayCollectionIterator();
    }

    private class MyArrayCollectionIterator implements Iterator<Integer> {

        private int position = 0;

        @Override
        public boolean hasNext() {

            if (position < size)
                return true;
            else
                return false;
        }

        @Override
        public Integer next() {

            if (this.hasNext())
                return a[position++];
            else
                return null;
        }
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {

        if (size < a.length)
            a[size] = (Integer) o;
        else
            enlarge();

        return true;
    }

    public void enlarge() {

        capacity *= 3;
        capacity /= 2;
        int[] array2 = new int[capacity];
        for (int i = 0; i < size; i++)
            array2[i] = a[i];
        a = new int[capacity];
        a = array2;
    }

    @Override
    public boolean remove(Object o) {

        for (int i = 0; i < size; i++)
            if (a[i] == (Integer) o)
                shift(i, true);

        return false;
    }

    public void shift(int i, boolean side) {

        if (side /*true - left, false - right*/) {
            for (int j = i; j < size; j++)
                a[j] = a[j + 1];
            size--;
        } else {
            enlarge();
            for (int j = i; j < size; j++)
                a[j + 1] = a[j];
            size++;
        }
    }

    @Override
    public boolean addAll(Collection collection) {

        for (Object o : collection)
            this.add(o);

        return true;
    }

    @Override
    public boolean addAll(int i, Collection collection) {

        int k = 0;

        for (Object o : collection) {
            if (k == i)
                this.add(o);
            k++;
        }

        return true;
    }

    @Override
    public void clear() {

        for (int i = 0; i < size; i++)
            a[i] = 0;
        size = 0;
    }

    @Override
    public Object get(int i) {

        return a[i];
    }

    @Override
    public Object set(int i, Object o) {

        a[i] = (int) o;
        return null;
    }

    @Override
    public void add(int i, Object o) {

        enlarge();
        shift(i, false);
        a[i] = (int) o;
    }

    @Override
    public Object remove(int i) {

        shift(i, true);

        return null;
    }

    @Override
    public int indexOf(Object o) {

        for (int i = 0; i < size; i++)
            if ((int) o == a[i])
                return 0;

        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {

        for (int i = size - 1; i >= 0; i++)
            if (a[i] == (int) o)
                return i;

        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int i) {
        return null;
    }

    @Override
    public List subList(int i, int i1) {

        if (i1 == a.length)
            return null;

        List arrayList = new MyArrayList();
        for (int k = i; k <= i1; k++)
            arrayList.add(a[k]);

        return arrayList;
    }

    @Override
    public boolean retainAll(Collection collection) {

        for (Object x : collection)
            if (contains(x))
                remove(x);

        return true;
    }

    @Override
    public boolean removeAll(Collection collection) {

        for (Object x : collection)
            remove(x);

        return false;
    }

    @Override
    public boolean containsAll(Collection collection) {

        for (Object o : collection)
            if (!contains(o))
                return false;

        return true;
    }

    @Override
    public Object[] toArray(Object[] objects) {
        return new Object[0];
    }
}
