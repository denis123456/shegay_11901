import java.util.*;

public class HTask23 {
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		System.out.print(replace(s));
	}
	
	public static String replace(String s) {
		
		String ans = "";
		int i = 0;
		int k = 0;
		int n = 0;
		boolean enter = false;
		
		while (i + 3 < s.length()) {
			if (check(s, i)) {
				k++;
				enter = true;
			}
			else
				enter = false;
			
			if (enter && k % 2 == 0) {
				ans += "false";
				i += 4;
				n++;
			}
			
			if (!enter || enter && k % 2 == 1) {
				ans += s.charAt(i);
				i++;
			}
		}
		
		if (s.length() + n > ans.length())
			ans += "" + s.charAt(i) + s.charAt(i + 1) + s.charAt(i + 2);
		
		return ans;
	}
	
	public static boolean check(String s, int i) {
		
		if (s.charAt(i) == 't' && s.charAt(i + 1) == 'r' && s.charAt(i + 2) == 'u' && s.charAt(i + 3) == 'e')
			return true;
		else
			return false;
	}
}