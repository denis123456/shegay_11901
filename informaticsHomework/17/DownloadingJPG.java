package urlDownload;

import java.io.*;
import java.net.URL;

public class DownloadingJPG {

    public static void main(String[] args) throws IOException {

        URL url = new URL("https://sun7-7.userapi.com/c824202/v824202845/126b84/9YPenH3UMhk.jpg?ava=1");
        InputStream in = new BufferedInputStream(url.openStream());
        OutputStream out = new BufferedOutputStream(new FileOutputStream("pic.jpg"));

        int i = in.read();
        while (i >= 0) {
            out.write(i);
            i = in.read();
        }
        in.close();
        out.close();
    }
}
