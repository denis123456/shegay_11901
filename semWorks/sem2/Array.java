package sem2;

public class Array {

    private int begin, end;

    public Array() {
        this(0, 0);
    }

    public Array(int begin, int count) {

        this.begin = begin;
        end = begin + count - 1;
    }

    public int getBegin() {

        return begin;
    }

    public int getEnd() {

        return end;
    }

    public void setEnd(int end) {

        this.end = end;
    }

    public int getSize() {

        return end - begin + 1;
    }
}
