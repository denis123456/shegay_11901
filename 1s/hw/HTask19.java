/**
* @author Denis Shegay
* 11-901
* Task 19
*/

import java.util.*;

public class HTask19 {
	
	public static void main(String args[]) {
		
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		String s1 = in.nextLine();
		System.out.print(isFirst(s, s1));
	}
	
	public static String isFirst(String s, String s1) {
		
		int i = 0;
		int min = 0;
		if (s.length() < s1.length())
			min = s.length();
		else
			min = s1.length();
		
		while (i < min) {
			if (s.charAt(i) < s1.charAt(i))
				return s;
			if (s.charAt(i) > s1.charAt(i))
				return s1;
			i++;
		}
		
		if (i == min)
			if (min == s.length())
				return s;
			else
				return s1;
		return s;
	}
}