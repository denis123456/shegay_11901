import java.util.regex.*;
import java.util.*;

public class HTask27 {

	public static void main(String[] args) {
	
		Scanner in = new Scanner(System.in);
		
		String k = in.nextLine();
		int n = Integer.parseInt(k);
		String s = "";
		Pattern p = Pattern.compile("((1)*|(0)*|([0][1]?)*|([0][1]?)*)");
		
		for (int i = 0; i < n; i++) {
			s = in.nextLine();
			Matcher m = p.matcher(s);
			if(m.matches()) 
				System.out.println(i+1);
		}
		
	}
}