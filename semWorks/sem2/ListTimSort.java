package sem2;

import java.util.LinkedList;
import java.util.Stack;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class ListTimSort {

    private static int iterations;
    private static LinkedList<Integer> list;

    public static void sort(LinkedList<Integer> listToSort) {

        iterations = 0;
        list = listToSort;
        int n = list.size();
        int minRun = getMinRun(n);
        boolean decreasing = false;
        boolean complemented;
        int decreaseTo = 0;
        int i = 0;
        Stack<Array> arrays = new Stack<>();
        int arraySize = 1;
        int stackCounter = 0;

        while (i + 1 < n) {

            if (list.get(i) <= list.get(i + 1)) {
                arraySize = up(list, i, arraySize);
            } else {
                arraySize = down(list, i, arraySize);
                decreasing = true;
                decreaseTo = i + arraySize - 1;
            }

            int tempI = i;
            int prevSize = arraySize;
            while (arraySize < minRun && tempI + prevSize < n) {
                arraySize++;
                tempI++;
            }

            complemented = tempI > i;

            Array x = new Array(i, arraySize);

            if (decreasing)
                reverse(list, x.getBegin(), decreaseTo);

            if (complemented)
                insertionSort(list, x.getBegin(), x.getEnd());

            if (stackCounter >= 2) {
                Array y = arrays.pop();
                Array z = arrays.pop();
                if (x.getSize() <= y.getSize() + z.getSize() || y.getSize() <= z.getSize()) {
                    Array array = x.getSize() < z.getSize() ? x : z;
                    if (array == x) {
                        arrays.push(z);
                        arrays.push(merge(array, y, list));
                    } else {
                        arrays.push(merge(y, array, list));
                        arrays.push(x);
                    }
                }
            } else {
                arrays.push(x);
                stackCounter++;
            }

            decreaseTo = 0;
            decreasing = false;
            arraySize = 1;
            i = arrays.peek().getEnd() + 1;
        }

        while (arrays.size() >= 2) {
            Array array1 = arrays.pop();
            Array array2 = arrays.pop();
            arrays.push(merge(array1, array2, list));
        }
    }

    public static Array merge(Array a1, Array a2, LinkedList<Integer> list) {

        boolean canGallop = false;
        int firstLeft = a1.getBegin();
        int firstRight = a1.getEnd();
        int secondLeft = a2.getBegin();
        int secondRight = a2.getEnd();
        int gallopThreshold = 7;
        int row = 0;
        int[] ans = new int[max(firstRight, secondRight) - min(firstLeft, secondLeft) + 1];
        int maxLeft, maxRight, minLeft, minRight, pos, added, currentDigit, counter;
        boolean maxIsFirst;
        boolean fromFirst = false;
        boolean fromSecond = false;
        int kLeft = 0; //index последнего не рассмотренного слева в ans
        int kRight = ans.length - 1; //index последнего не рассмотренного справа в ans

        while ((firstLeft <= firstRight || secondLeft <= secondRight) && kLeft <= kRight) {
            if (canGallop) {
                if (firstRight - firstLeft > secondRight - secondLeft) {
                    maxLeft = firstLeft;
                    maxRight = firstRight;
                    minLeft = secondLeft;
                    minRight = secondRight;
                } else {
                    maxLeft = secondLeft;
                    maxRight = secondRight;
                    minLeft = firstLeft;
                    minRight = firstRight;
                }

                if (minLeft > minRight) {
                    for (int i = maxLeft; i <= maxRight; i++)
                        ans[kLeft + i - maxLeft] = list.get(i);
                    break;
                }

                added = 0;
                counter = 0;
                maxIsFirst = firstLeft == maxLeft;

                //left
                pos = binarySearchLeft(list, list.get(minLeft), maxLeft, maxRight);
                //все до индекса pos меньше чем a[minleft]
                if (pos != -1) {
                    for (int i = maxLeft; i <= pos; i++) {
                        ans[kLeft - maxLeft + i] = list.get(i);
                        counter++;
                    }
                    kLeft += counter;
                }
                ans[kLeft] = list.get(minLeft);
                kLeft++;
                added++;

                if (maxIsFirst) {
                    firstLeft += counter;
                    secondLeft++;
                } else {
                    firstLeft++;
                    secondLeft += counter;
                }


                if (kLeft <= kRight) {
                    counter = 0;
                    pos = binarySearchRight(list, list.get(minRight), maxLeft, maxRight);

                    if (pos != -1) {
                        for (int i = maxRight; i >= pos; i--) {
                            ans[kRight - maxRight + i] = list.get(i);
                            counter++;
                        }
                        kRight -= counter;
                    }
                    ans[kRight] = list.get(minRight);
                    kRight--;
                    added++;

                    if (maxIsFirst) {
                        firstRight -= counter;
                        secondRight--;
                    } else {
                        firstRight--;
                        secondRight -= counter;
                    }
                }

                canGallop = added >= gallopThreshold;
                iterations += added;
                row = 0;
            } else {
                if (firstLeft <= firstRight && secondLeft <= secondRight)
                    currentDigit = min(list.get(firstLeft), list.get(secondLeft));
                else
                    currentDigit = firstLeft > firstRight ? list.get(secondLeft) : list.get(firstLeft);

                if (firstLeft <= firstRight && currentDigit == list.get(firstLeft)) {
                    firstLeft++;
                    ans[kLeft] = currentDigit;
                    kLeft++;
                    iterations++;
                    if (fromFirst)
                        row++;
                    else
                        row = 1;
                    fromFirst = true;
                    fromSecond = false;
                } else {
                    ans[kLeft] = currentDigit;
                    kLeft++;
                    iterations++;
                    secondLeft++;
                    if (fromSecond)
                        row++;
                    else
                        row = 1;
                    fromFirst = false;
                    fromSecond = true;
                }

                canGallop = row == gallopThreshold;
            }
        }

        int begin = min(a1.getBegin(), a2.getBegin());
        for (int i = 0; i < ans.length; i++)
            list.set(i + begin, ans[i]);
        iterations += ans.length - begin + 1;

        return new Array(begin, ans.length);
    }

    public static void reverse(LinkedList<Integer> list, int begin, int end) {

        int n = end - begin + 1;
        int temp;

        for (int i = 0; i < n / 2; i++) {
            temp = list.get(i + begin);
            iterations++;
            list.set(i + begin, list.get(n + begin - i - 1));
            list.set(n + begin - i - 1, temp);
        }
    }

    public static void insertionSort(LinkedList<Integer> list, int begin, int end) {

        iterations++;
        for (int i = begin; i <= end; i++) {
            int key = list.get(i);
            int j = i - 1;

            while (j >= begin && list.get(j) > key) {
                list.set(j + 1, list.get(j));
                iterations++;
                j = j - 1;
            }

            list.set(j + 1, key);
        }
    }

    public static int binarySearchLeft(LinkedList<Integer> list, int key, int begin, int end) { //все до a[index] <= key

        int mid;
        int constEnd = end;
        if (list.get(begin) > key)
            return -1;

        while (begin < end) {
            iterations++;
            mid = (begin + end) / 2;
            if (key > list.get(mid))
                begin = mid + 1;
            else if (key < list.get(mid))
                end = mid;
            else {
                while (constEnd >= mid && list.get(mid + 1) == key)
                    mid++;
                return mid;
            }
        }

        if (key < list.get(begin))
            return begin - 1;
        else
            return begin;
    }

    public static int binarySearchRight(LinkedList<Integer> list, int key, int begin, int end) {

        int mid;
        int constBegin = begin;
        if (list.get(end) < key)
            return -1;

        while (begin < end) {
            iterations++;
            mid = (begin + end) / 2;
            if (key > list.get(mid))
                begin = mid + 1;
            else if (key < list.get(mid))
                end = mid;
            else {
                while (mid > constBegin && list.get(mid - 1) == key)
                    mid--;
                return mid;
            }
        }

        if (key > list.get(begin))
            return begin + 1;
        else
            return begin;
    }

    public static int down(LinkedList<Integer> list, int index, int arraySize) {

        if (list.get(index) > list.get(index + 1)) {
            arraySize++;
            index++;
            if (index < list.size() - 1)
                arraySize = down(list, index, arraySize);
        }

        return arraySize;
    }

    public static int up(LinkedList<Integer> list, int index, int arraySize) {

        if (list.get(index) <= list.get(index + 1)) {
            arraySize++;
            index++;
            if (index + 1 < list.size())
                arraySize = up(list, index, arraySize);
        }

        return arraySize;
    }

    public static int getMinRun(int n) {

        int r = 0;

        while (n >= 64) {
            iterations++;
            r |= n & 1;
            n >>= 1;
        }

        return n + r;
    }

    public static int getIterations() {

        return iterations;
    }

    public static boolean checkSorted() {

        int[] array = list.stream().mapToInt(e -> e).toArray();
        for (int i = 0; i < array.length - 1; i++)
            if (array[i] > array[i + 1])
                return false;

        return true;
    }

}
