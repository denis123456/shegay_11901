package myCollections;

import org.w3c.dom.Node;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyLinkedList implements List {

    private Node head;
    private Node end;
    private int size;

    static class Node {

        int val;
        Node next;

        public Node(int val) {

            this.val = val;
            next = null;
        }

        public Node() {

            val = 0;
            next = null;
        }

        public Node getNext() {

            return this.next;
        }

        public int getVal() {

            return val;
        }

        public boolean hasNext() {

            return !(this.next == null);
        }
    }

    public MyLinkedList() {

        head = new Node();
        end = head;
        size = 0;
    }

    public MyLinkedList(int val) {

        head = new Node(val);
        end = head;
        size = 1;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    @Override
    public boolean contains(Object o) {

        Node current = head;
        while (current.hasNext()) {
            if (current.getVal() == (int) o)
                return true;
            head = head.next;
        }

        if (current.getVal() == (int) o)
            return true;

        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    private class MyArrayCollectionIterator implements Iterator<Integer> {

        Node current = head;

        @Override
        public boolean hasNext() {

            return current.hasNext();
        }

        @Override
        public Integer next() {

            if (this.hasNext())
                return current.getVal();
            else
                return null;

        }
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {

        end.next = new Node((int) o);
        end = end.next;
        return true;
    }

    @Override
    public boolean remove(Object o) {

        Node current = head;
        while (current.hasNext()) {
            if (current.next.getVal() == (int) o) {
                current.next = current.next.next;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean addAll(Collection collection) {

        for (Object o : collection)
            add(o);

        return true;
    }

    @Override
    public boolean addAll(int i, Collection collection) {


        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Object get(int i) {

        int k = 0;
        Node current = head;

        while (current.hasNext() && k < i) {
            current = current.next;
            k++;
        }

        return current.getVal();
    }

    @Override
    public Object set(int i, Object o) {

        int k = 0;
        Node current = head;

        while (current.hasNext() && k < i) {
            current = current.next;
            k++;
        }

        current.val = (int) o;

        return null;
    }

    @Override
    public void add(int i, Object o) {

        Node current = head;
        int k = 0;

        while (current.hasNext() && k < i - 1) {
            current = current.next;
            k++;
        }

        Node node = new Node((int) o);
        node.next = current.next;
        current.next = node;

    }

    @Override
    public Object remove(int i) {

        Node current = head;
        int k = 0;

        while (current.hasNext() && k < i - 1) {
            current = current.next;
            k++;
        }

        current = current.next.next;

        return null;
    }

    @Override
    public int indexOf(Object o) {

        Node current = head;
        int k = 0;

        while (current.hasNext()) {
            if (current.getVal() == (int) o)
                return k;
            k++;
        }

        if (current.getVal() == (int) o)
            return k;

        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {

        Node current = head;
        int k = 0;
        int pos = -1;

        while (current.hasNext()) {
            if (current.getVal() == (int) o)
                pos = k;
            k++;
        }

        if (current.getVal() == (int) o)
            return k;

        return pos;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int i) {
        return null;
    }

    @Override
    public List subList(int i, int i1) {

        MyLinkedList list = new MyLinkedList();
        Node current = head;
        int k = 0;

        while (current.hasNext()) {
            if (k >= i && k <= i1)
                list.add(current);
            k++;
        }

        return null;
    }

    @Override
    public boolean retainAll(Collection collection) {

        for (Object o : collection)
            if (contains(o))
                remove((int) o);

        return false;
    }

    @Override
    public boolean removeAll(Collection collection) {

        for (Object o : collection)
            remove((int) o);
        return false;
    }

    @Override
    public boolean containsAll(Collection collection) {

        for (Object o : collection)
            if (!contains(o))
                return false;

        return true;
    }

    @Override
    public Object[] toArray(Object[] objects) {
        return new Object[0];
    }
}
