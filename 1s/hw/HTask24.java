import java.util.*;

public class HTask24 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		System.out.print(replace(s));
	}
	
	public static String replace(String s) {
		
		String ans = "";
		int i = 0;
		
		while (i + 2 < s.length()) {
			if (check(s, i)) {
				ans += "mom";
				i += 3;
			}
			else {
				ans += s.charAt(i);
				i++;
			}
		}
		
		if (s.length() > ans.length())
			ans += "" + s.charAt(i) + s.charAt(i + 1);
		   
		return ans;
	}
	
	public static boolean check(String s, int i) {
		
		if (s.charAt(i) == 'd' && s.charAt(i + 1) == 'a' && s.charAt(i + 2) == 'd')
			return true;
		else
			return false;
	}
}