import java.util.Random;

public class HTask36_Player {

	int hp;
	String name;
	final Random random = new Random();
	int chance = 0;
	
	Player(String name) {
		
		this.name = name;
		hp = 100;
	}
	
	void attack(Player target, int damage) {
		
		chance = random.nextInt(10) + 1;
		
		if (chance < 10 && chance >= damage) {
			target.hp -= damage;
			System.out.println(target.name + " loses " + damage + " hp!");
		}
		else
			System.out.println(this.name + " missed!");
	}
	
}