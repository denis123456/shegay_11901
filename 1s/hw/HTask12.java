/**
* @author Denis Shegay
* 11-901
* Task12
*/
import java.util.*;
import java.io.*;

public class HTask12{
	
	public static void main(String args[]){
		
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = 0;
		while (n != 0){
			if (n % 10 == 5)
				k = 1;
			n /= 10;
		}
		if (k == 1)
			System.out.print("yes");
		else
			System.out.print("no");
	}
}