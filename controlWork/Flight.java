package controlWork;

public class Flight {

    private int airline;
    private int number;
    private String source;
    private String destination;

    public Flight(int airline, int number, String source, String destination) {

        this.airline = airline;
        this.number = number;
        this.source = source;
        this.destination = destination;
    }

    public int getAirline() {

        return airline;
    }

    public String getSource() {

        return source;
    }

    public String getDestination() {

        return destination;
    }
}
