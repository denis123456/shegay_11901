/**
* @author Denis Shegay
* 11-901
* Task 15
*/
import java.util.*;
import java.io.*;

public class HTask15 {
	
	public static void main(String args[]) {
	 
		Scanner in = new Scanner(System.in);
		final double C = 1e-9;
		double x0 = in.nextDouble();
		double x = 1;
		double s = 0;
		int f = 1;
		int k = 1;
		double a = 1.0;
		while (Math.abs(a) > 1e-9) {
			s += a;
			x *= x0;
			f *= k++;
			a = x/f;
		}
		System.out.print(s);
	}
}