public class Rectangle {

	private double[] x = new double[4];
	private double[] y = new double[4];
	private double a, b, c;
	
	public Rectangle(double x1, double y1, double x2, double y2, 
	double x3, double y3, double x4, double y4) {
		
		this.x[0] = x1;
		this.y[0] = y1;
		this.x[1] = x2;
		this.y[1] = y2;
		this.x[2] = x3;
		this.y[2] = y3;
		this.x[3] = x4;
		this.y[3] = y4;
		a = Math.sqrt((x[0] - x[1])*(x[0] - x[1]) + (y[0] - y[1])*(y[0] - y[1]));
		b = Math.sqrt((x[0] - x[2])*(x[0] - x[2]) + (y[0] - y[2])*(y[0] - y[2]));
		c = Math.sqrt((x[0] - x[3])*(x[0] - x[3]) + (y[0] - y[3])*(y[0] - y[3]));
	}
	
	public void widlen() {
		
		if (b >= c && b >= a)
			this.b = c;
		else
		if (a >= c && a >= b)
			this.a = c;
	}
	
	public double square() {
		
		this.widlen();
		return a * b;
	}
	
	public double perimeter() {
		
		this.widlen();
		return 2 * (a + b);
	}
	
	public boolean crosses(Rectangle r) {
		
		double square = this.square();
		double square2 = 0;
		boolean check = false;
		int i = 0;
		
		while (!check && i < 4) {
			square2 = 0;
			
			for (int j = 0; j < 4; j++)
				square2 += 0.5 * Math.abs((x[j % 4] - r.x[i]) * (y[(j + 1) % 4] - r.y[i])
										- (x[(j + 1) % 4] - r.x[i]) * (y[j % 4] - r.y[i]));
	
			if (square == square2)
				check = true;
			i++;
		}
		
		return check;
	}
		
	public boolean equals(Rectangle r) {
		
		boolean check = true;
		
		for (int i = 0; i < 4; i++)
			if (x[i] != r.x[i] || y[i] != r.y[i])
				check = false;
			
		return check;
	}
		
	public String toString() {
		
		double length = this.a;
		double width = this.b;
		
		if (length < this.b) {
			length = width;
			width = this.a;
		}
		
		return "" + "Length is: " + length + "\n" + 
				"Width is: " + width;
	}
}