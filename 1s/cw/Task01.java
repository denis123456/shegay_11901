/**
* CW variant 2
* @author Denis Shegay 
* 11-901
* Task 01
*/

import java.util.*;
import java.io.*;

public class Task01{
	
	public static void main(String args[]) {
	
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] a = new int[n];
		int i = 0;
		int x = 0;
		boolean check = true;
		while (check && i < n) {
			a[i] = in.nextInt();
			check = false;
			x = a[i++];
			while (x != 0) {
				if (x % 10 == 3 || x % 10 == 5)
					check = true;
				x /= 10;
			}
		}
		if (check)
			System.out.print("true");
		else
			System.out.print("false");
	}
}