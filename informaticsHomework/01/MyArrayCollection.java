package myCollections;

import java.util.Collection;
import java.util.Iterator;

public class MyArrayCollection implements Collection<Integer> {

    private int[] array;
    private int size;
    private int capacity;

    public MyArrayCollection() {

        capacity = 1000;
        array = new int[capacity];
        size = 0;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    @Override
    public boolean contains(Object o) {

        for (int i = 0; i < size; i++)
            if (array[i] == (int) o)
                return true;

        return false;
    }

    @Override
    public Iterator<Integer> iterator() {

        return new MyArrayCollectionIterator();
    }

    private class MyArrayCollectionIterator implements Iterator<Integer> {

        private int position = 0;

        @Override
        public boolean hasNext() {

            if (position < size)
                return true;
            else
                return false;
        }

        @Override
        public Integer next() {

            if (this.hasNext())
                return array[position++];
            else
                return null;
        }
    }

    @Override
    public boolean add(Integer x) {

        if (size == capacity)
            enlarge();
        size++;
        array[size] = x;

        return true;
    }

    public void enlarge() {

        capacity += 10;
        int[] array2 = new int[capacity];
        for (int i = 0; i < size; i++)
            array2[i] = array[i];
        array = new int[capacity];
        array = array2;
    }

    @Override
    public boolean remove(Object o) {

        for (int i = 0; i < size; i++)
            if (array[i] == (int) o)
                shift(i);

        return false;
    }

    public void shift(int i) {

        for (int j = i; j < size; j++)
            array[j] = array[j + 1];
        size--;
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        for (Object x : c)
            if (!contains(x))
                return false;

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {

        for (Integer x : c)
            this.add(x);

        return false;
    }

    public int getSize() {

        return size;
    }

    @Override
    public boolean removeAll(Collection<?> c) {

        for (Object x : c)
            remove(x);

        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        for (Object x : c)
            if (contains(x))
                remove(x);

        return true;
    }

    @Override
    public void clear() {

        for (int i = 0; i < size; i++)
            array[i] = 0;
        size = 0;
    }

    @Override
    public Object[] toArray() {

        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] old) {

        return null;
    }

}