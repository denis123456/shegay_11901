package sem1;

public class Participant {

    private String name, gender;

    public Participant(String name, String gender) {

        this.name = name;
        this.gender = gender;
    }

    public Participant() {

        this("", "");
    }

    public String getName() {

        return name;
    }

    public String getGender() {

        return gender;
    }

    public String toString() {

        return name + ", " + gender;
    }
}
