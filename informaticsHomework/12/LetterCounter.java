package tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LetterCounter {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("C:\\Users\\densh\\IdeaProjects\\classwork\\src\\text");
        Scanner in = new Scanner(file);
        Map<Character, Integer> map = new HashMap<>();
        String s;
        while (in.hasNextLine()) {
            s = in.nextLine().toLowerCase();
            for (Character character : s.toCharArray())
                map.put(character, map.get(character) == null ? 1 : map.get(character) + 1);
        }
        map.entrySet().forEach(System.out::println);
    }
}
