/**
* CW variant 2
* @author Denis Shegay 
* 11-901
* Task 03
*/

import java.util.*;
import java.io.*;

public class Task03 {
	
	public static void main(String args[]) {
		
		Scanner in = new Scanner(System.in);
		int[] yaneponyaluslovie = new int[15];
		int n = in.nextInt();
		int x = 0;
		for (int i = 0; i < n*n; i++) {
			x = in.nextInt();
			yaneponyaluslovie[x - 7]++;
		}
		for (int i = 0; i < 15; i++) 
			System.out.println(i+7+":00: "+yaneponyaluslovie[i]+" workers");
	}
}