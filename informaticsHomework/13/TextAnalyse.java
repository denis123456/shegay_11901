package tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class TextAnalyse {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("C:\\Users\\densh\\IdeaProjects\\classwork\\src\\textAnalyse");
        Scanner in = new Scanner(file);
        List<Character> list = new LinkedList<>();
        String s;
        while (in.hasNextLine()) {
            s = in.nextLine().toLowerCase();
            for (Character chr : s.toCharArray())
                list.add(chr);
        }


        Map<Character, Integer> map =
                list.stream()
                        .filter(x -> (x <= 'z' && x >= 'a'))
                        .collect(Collectors.toMap(a -> a, b -> 1, Integer::sum));

        System.out.println(map);
    }
}
