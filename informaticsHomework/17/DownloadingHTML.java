package urlDownload;

import java.io.*;
import java.net.URL;

public class DownloadingHTML {

    public static void main(String[] args) throws IOException {

        URL url = new URL("https://vk.com/feed");
        InputStream in = new BufferedInputStream(url.openStream());
        OutputStream out = new BufferedOutputStream(new FileOutputStream("page2.html"));

        int i = in.read();
        while (i >= 0) {
            out.write(i);
            i = in.read();
        }
        in.close();
        out.close();
    }
}
