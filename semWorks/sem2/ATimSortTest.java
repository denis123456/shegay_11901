package sem2;

import org.junit.Assert;
import org.junit.Test;
import static sem2.ATimSort.*;

public class ATimSortTest {

    @Test
    public void binarySearchLeftTest() {

        int[] a = {2, 2, 2, 2, 2, 4};
        int pos = binarySearchLeft(a, 2, 0, 4);
        Assert.assertEquals(5, pos);
        pos = binarySearchLeft(a, 1, 0, 5);
        Assert.assertEquals(-1, pos);
        pos = binarySearchLeft(a, 3, 0, 5);
        Assert.assertEquals(4, pos);
        pos = binarySearchLeft(a, 4, 0,5);
        Assert.assertEquals(5,pos);
        pos = binarySearchLeft(a, 5,0,5);
        Assert.assertEquals(5, pos);
    }

    @Test
    public void binarySearchRightTest() {

        int[] a = {4, 5, 5, 5, 6, 9};
        int pos = binarySearchRight(a, 4, 0, 5);
        Assert.assertEquals(0, pos);
        pos = binarySearchRight(a, 9, 0, 5);
        Assert.assertEquals(5, pos);
        pos = binarySearchRight(a, 7, 0, 5);
        Assert.assertEquals(5, pos);
        pos = binarySearchRight(a, 6, 0, 5);
        Assert.assertEquals(4, pos);
        pos = binarySearchRight(a, 10, 0, 5);
        Assert.assertEquals(-1, pos);
    }

    @Test
    public void insertionSortTest() {

        int[] a = {9, 8, 7, 6, 10, 9, 8, 7, 6, 5};
        int[] expected = {6, 7, 8, 9, 5, 6, 7, 8, 9, 10};
        insertionSort(a, 0, 3);
        insertionSort(a,4,a.length - 1);
        Assert.assertEquals("equal", arrayCompare(a, expected));
    }

    @Test
    public void reverseTest() {

        int[] a = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        reverse(a,0, 9);
        Assert.assertEquals("equal", arrayCompare(a, expected));
    }

    @Test
    public void upTest() {

        int[] a = {1, 2, 0};
        int arraySize = 1;
        arraySize = up(a, 0, arraySize);
        Assert.assertEquals(2, arraySize);
    }

    @Test
    public void arrayGetBeginTest() {

        Array array = new Array(4, 3);
        Assert.assertEquals(4, array.getBegin());
    }

    @Test
    public void arrayGetEndTest() {

        Array array = new Array(4, 3);
        Assert.assertEquals(6, array.getEnd());
    }

    public String arrayCompare(int[] a, int[] b) {

        if (a.length != b.length)
            return a.length > b.length ? "first is bigger" : "second is bigger";

        for (int i = 0; i < a.length; i++)
            if (a[i] != b[i])
                return "elements with index " + i + " are not equal";

        return "equal";
    }

    public int[] arrayCopy(int[] a) {

        int[] ans = new int[a.length];

        System.arraycopy(a, 0, ans, 0, a.length);

        return ans;
    }
}
