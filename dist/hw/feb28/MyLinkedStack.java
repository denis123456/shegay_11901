package practice.feb21;

public class MyLinkedStack<T> implements IMyStack<T>{

    private Elem head;

    @Override
    public void push(T element) {

        Elem x = new Elem();
        x.value = element;
        x.next = head;
        head = x;
    }

    @Override
    public void pop() {

        head = head.next;
    }

    @Override
    public T peek() {

        return (T) head.value;
    }

    @Override
    public boolean isEmpty() {

        return head == null;
    }

}
