import java.util.Scanner;
import java.util.regex.*;

public class HTask26 {

	public static void main (String[] args) {

		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		Pattern p = Pattern.compile("(-?(0((([,]|[.]){1}[0]*[1-9]+(/((([0-9]*/))))?))|(([,]|[.]){1}[1-9]+(/((([0-9]*/))))?))|[1-9]*(([,]|[.]){1}[0]*[1-9]+(/((([0-9]*/))))?)|(([,]|[.]){1}[1-9]+(/((([0-9]*/))))?))");
		Matcher m = p.matcher(s);
		if (m.matches())
			System.out.println("Real");
		else
			System.out.println("Not real");
	}
}