package streamAPI;

public class Subscription {

    private User subscriber;
    private User user;

    public Subscription(User subscriber, User user) {

        this.subscriber = subscriber;
        this.user = user;
    }

    public User getSubscriber() {

        return subscriber;
    }

    public User getUser() {

        return user;
    }
}
