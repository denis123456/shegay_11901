public class HTask47_RationalComplexMatrix2x2 {

	private RationalComplexNumber[][] a = new RationalComplexNumber[2][2];
	
	public RationalComplexMatrix2x2() {
		
		this(new RationalComplexNumber(),
			 new RationalComplexNumber(),
			 new RationalComplexNumber(),
			 new RationalComplexNumber());
	}
	
	public RationalComplexMatrix2x2(RationalComplexNumber a) {
		
		this(a, a, a, a);
	}
	
	public RationalComplexMatrix2x2
	(RationalComplexNumber a, RationalComplexNumber b,
	 RationalComplexNumber c, RationalComplexNumber d) {
		
		this.a[0][0] = a;
		this.a[0][1] = b;
		this.a[1][0] = c;
		this.a[1][1] = d;
	}
	 
	public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 matrix) {

		RationalComplexMatrix2x2 newMatrix =
		new RationalComplexMatrix2x2();
			
		newMatrix.a[0][0] = this.a[0][0].add(matrix.a[0][0]);
		newMatrix.a[0][1] = this.a[0][1].add(matrix.a[0][1]);
		newMatrix.a[1][0] = this.a[1][0].add(matrix.a[1][0]);
		newMatrix.a[1][1] = this.a[1][1].add(matrix.a[1][1]);
		
		return newMatrix;
	}
	
	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 matrix) {
		
		RationalComplexMatrix2x2 newMatrix =
		new RationalComplexMatrix2x2();
		
		newMatrix.a[0][0] = 
		(this.a[0][0].mult(matrix.a[0][0])).add(this.a[0][1].mult(matrix.a[1][0]));
		newMatrix.a[0][1] = 
		(this.a[0][0].mult(matrix.a[0][1])).add(this.a[0][1].mult(matrix.a[1][1]));
		newMatrix.a[1][0] = 
		(this.a[1][0].mult(matrix.a[0][0])).add(this.a[1][1].mult(matrix.a[1][0]));
		newMatrix.a[1][1] = 
		(this.a[1][0].mult(matrix.a[0][1])).add(this.a[1][1].mult(matrix.a[1][1]));
		
		return newMatrix;
	}
	
	public RationalComplexNumber det() {
		
		RationalComplexNumber newNumber =
		new RationalComplexNumber();
		
		RationalComplexNumber a = this.a[0][0].mult(this.a[1][1]);
		RationalComplexNumber b = this.a[0][1].mult(this.a[1][0]);
		
		newNumber.setA(a.getA().add(b.getA()));
		newNumber.setB(a.getB().add(b.getB()));
		
		return newNumber;
	}
	
	public RationalComplexVector2D multVector(RationalComplexVector2D vector) {
		
		RationalComplexVector2D newVector = new RationalComplexVector2D();
		
		newVector.setA(vector.getA().mult(this.a[0][0]).add(vector.getB().mult(this.a[0][1])));
		newVector.setB(vector.getA().mult(this.a[1][0]).add(vector.getB().mult(this.a[1][1])));
		
		return newVector;
	}

	public String toString() {
		
		return "" + this.a[0][0] + " " + this.a[0][1] + "\n" 
				+ this.a[1][0] + " " + this.a[1][1];
	}
}