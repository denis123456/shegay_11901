package myCollections;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyLinkedCollection implements Collection<Integer> {

    private List<Integer> list;

    public MyLinkedCollection() {

        list = new LinkedList<>();
    }

    @Override
    public int size() {

        return list.size();
    }

    @Override
    public boolean isEmpty() {

        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {

        return list.contains(o);
    }

    @Override
    public Iterator<Integer> iterator() {

        return new MyListCollectionIterator();
    }

    private class MyListCollectionIterator implements Iterator<Integer> {

        private int position = 0;

        @Override
        public boolean hasNext() {

            if (position < list.size())
                return true;
            else
                return false;
        }

        @Override
        public Integer next() {

            if (this.hasNext())
                return list.get(position++);
            else
                return null;
        }
    }

    @Override
    public Object[] toArray() {

        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {

        return null;
    }

    @Override
    public boolean add(Integer integer) {

        return list.add(integer);
    }

    @Override
    public boolean remove(Object o) {

        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {

        for (Object o : collection)
            if (!this.contains(o))
                return false;

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> collection) {

        for (Integer x : collection)
            if (!this.add(x))
                return false;

        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {

        for (Object o : collection)
            this.remove(o);

        return true;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {

        for (Object x : collection)
            if (contains(x))
                remove(x);

        return true;
    }

    @Override
    public void clear() {

        for (Integer i : list)
            remove(i);
    }
}
