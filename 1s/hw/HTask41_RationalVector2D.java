public class HTask41_RationalVector2D {
	
	private RationalFraction a, b;
	
	public RationalVector2D() {
		
		this(new RationalFraction(), new RationalFraction());
	}
	
	public RationalVector2D(RationalFraction a, RationalFraction b) {
		
		this.a = a;
		this.b = b;
	}
	
	public RationalVector2D add(RationalVector2D vector) {
		
		RationalVector2D newVector = 
		new RationalVector2D(this.a.add(vector.a), this.b.add(vector.b));
		
		return newVector;
	}
	
	public String toString() {
		
		return "{" + this.a.toString() + "; " + this.b.toString() + "}";
	}
	
	public double length() {
		
		return Math.sqrt((this.a.value())*(this.a.value()) + (this.b.value())*(this.b.value()));
	}
	
	public RationalFraction getA() {
		
		return this.a;
	}
	
	public RationalFraction getB() {
		
		return this.b;
	}
	
	public void setA(RationalFraction a) {
		
		this.a = a;
	}
	
	public void setB(RationalFraction b) {
		
		this.b = b;
	}
	
	public RationalFraction scalarProduct(RationalVector2D vector) {
		
		return this.a.mult(vector.a).add(this.b.mult(vector.b));
	}
	
	public boolean equals(RationalVector2D vector) {
		
		if (this.a.equals(vector.a) && this.b.equals(vector.b))
			return true;
		else
			return false;
	}
}
	