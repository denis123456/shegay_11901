/**
* @author Denis Shegay
* 11-901
* Task 16
*/
import java.util.*;
import java.io.*;

public class HTask16 {
	
	public static void main(String args[]) {
	 
		Scanner in = new Scanner(System.in);
		final double C = 1e-9;
		double x0 = in.nextDouble();
		double x = x0;
		int m = 1;
		double s = 0;
		int k = 1;
		double a = x;
		while (Math.abs(a) >= C) {
			s += a;
			m *= -1;
			x *= x0;
			a = x/++k;
		}
		System.out.print(s);
	}
}