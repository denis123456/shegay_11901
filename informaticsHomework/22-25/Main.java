package streamAPI;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        User user = new User("Name", "Kazan", 2000);
        Message message = new Message(user, user, "txt");
        LinkedList<Message> messages = new LinkedList<>();
        messages.add(message);
        System.out.println(first(messages));

        User user2 = new User("Name2", "Kazan", 2000);
        Message message2 = new Message(user2, user2, "txt");
        Message message3 = new Message(user2, user, "txt");
        messages.add(message2);
        messages.add(message3);
        System.out.println(second(messages));

        User user3 = new User("Name3", "Moscow", 2000);
        LinkedList<User> users = new LinkedList<>();
        users.add(user);
        users.add(user2);
        users.add(user3);
        user.subscribe(user3);
        user2.subscribe(user3);
        System.out.println(third(users));
        //LinkedList<Subscription> subs = new LinkedList<>();
        //subs.add(new Subscription(user, user3));
        //subs.add(new Subscription(user2, user3));
        //System.out.println(third(subs));
    }

    public static long first(LinkedList<Message> messages) {

        return messages.stream().map(message -> message.getSender().getCity()).filter(city -> city.equals("Kazan")).count();
    }

    public static List<User> second(LinkedList<Message> messages) {

        Map<User, Long> sent =
                messages
                        .stream()
                        .collect(Collectors
                                .groupingBy(Message::getSender, Collectors.counting()));

        Map<User, Long> received =
                messages
                        .stream()
                        .collect(Collectors
                                .groupingBy(Message::getReceiver, Collectors.counting()));

        List<User> list = sent
                .entrySet()
                .stream()
                .map(e -> (e.getValue() > received.get(e.getKey()) ? e.getKey() : null))
                .filter(e -> e != null)
                .collect(Collectors.toList());

        return list;
    }

    public static List<User> third(LinkedList<User> users /*LinkedList<Subscription> subs*/) {

        /*List<User> list = subs.stream()
                .filter(e -> (subs.stream().filter(x -> x.getSubscriber().getAge() >= 18)
                        .collect(Collectors.toList()).size() >
                        subs.stream().filter(y -> y.getSubscriber().getAge() < 18)
                .collect(Collectors.toList()).size()))
                .collect(Collectors.toList())
                .stream()
                .filter(z -> (subs.stream().
                        filter(o -> (o.getSubscriber().getCity()
                                .equals(z.getSubscriber().getCity())))
                        .collect(Collectors.toList()).size() < subs.size() -
                        (subs.stream().
                                filter(o -> (o.getSubscriber().getCity()
                                        .equals(z.getSubscriber().getCity())))
                                .collect(Collectors.toList()).size()))).map(s -> s.getUser())
                .collect(Collectors.toList());*/

        List<User> list = users.stream()
                .filter(e -> (e.getSubscribers().size() -
                        e.getSubscribers().stream().filter(user -> user.getAge() >= 18)
                                .collect(Collectors.toList()).size() <
                        e.getSubscribers().stream().filter(user -> user.getAge() >= 18)
                        .collect(Collectors.toList()).size()))
                .collect(Collectors.toList()).stream()
                .filter(x -> (x.getSubscribers().size() >
                        x.getSubscribers().stream().filter(z -> z.getCity().equals(x.getCity()))
                                .collect(Collectors.toList()).size()))
                .collect(Collectors.toList());

        return list;
    }

    public static List fourth(LinkedList<Message> messages) {

        List<Message> list = messages.stream().filter(m -> !m.getStatus()).collect(Collectors.toList());
        list.stream().collect(Collectors.toMap(Message::getSender, Message::getReceiver));

        return null;
    }
}
