package reflection2;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {

        Scanner in = new Scanner(System.in);
        String type = in.nextLine();
        Class<? extends Converter> c = (Class<? extends Converter>) Class.forName(type);
        Converter converter = c.newInstance();
        Formatter formatter = new Formatter(converter);
        BufferedReader br = new BufferedReader(
                                new InputStreamReader(
                                        new FileInputStream(".src/input.txt")));
        PrintWriter pw = new PrintWriter(new FileOutputStream(".src/output.html"), true);
        formatter.open(pw);
        if (type.equals("LiConverter"))
            formatter.addTag(pw);
        formatter.format(br, pw);
        if (type.equals("LiConverter"))
            formatter.addTag(pw);
        formatter.close(pw);
        br.close();
        pw.close();
    }
}
