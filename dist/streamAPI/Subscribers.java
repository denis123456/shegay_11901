package streamAPI;

import java.util.LinkedList;

public class Subscribers {

    private LinkedList<User> subs;

    public Subscribers() {

        subs = new LinkedList<>();
    }

    public LinkedList<User> getSubs() {

        return this.subs;
    }
}
