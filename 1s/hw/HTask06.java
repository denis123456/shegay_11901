/**
* @author Denis Shegay
* 11-901
* Task 06
*/
import java.util.*;
import java.io.*;

public class HTask06{

	public static void main(String args[]){

		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int k = 0; k < n; k++){
			for (int i = 0; i < n - k - 1; i++)
				System.out.print(" ");
			for (int i = 0; i < 1 + 2*k; i++)
				System.out.print(1);
			System.out.println();
		}
	}
}
