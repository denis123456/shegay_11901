import java.util.Random;
import java.util.Scanner;
import java.util.regex.*;

public class HTask28 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		Random random = new Random();
		Pattern p = Pattern.compile("-?([13579]*([02468]{0,2})[1357]*)");
		int i = 0;
		int k = 0;
		int x = 0;
		String s = "";
		
		while (i < 10) {
			x = random.nextInt(1000);
			s = s.valueOf(x);
			Matcher m = p.matcher(s);
			if (m.matches()) {
				i++;
				System.out.println(s);
			}
			k++;
		}
		
		System.out.println("Digits generated: " + k);
	}
}