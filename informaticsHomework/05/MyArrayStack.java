package myCollections;

public class MyArrayStack<T> {

    private T[] a;
    private int size, capacity;

    public MyArrayStack() {

        capacity = 10;
        a = (T[]) new Object[capacity];
        size = 0;
    }

    public void push(T o) {

       if (size == capacity)
           enlarge();
       a[size] = o;
       size++;
    }

    public Object pop() {

        Object ans = a[size];
        a[size] = null;
        size--;

        return ans;
    }

    public boolean isEmpty() {

        return size == 0;
    }

    public T peek() {

        return a[Math.max(size - 1, 0)];
    }

    public void enlarge() {

        capacity *= 3;
        capacity /= 2;
        T[] array2 = (T[]) new Object[capacity];
        for (int i = 0; i < size; i++)
            array2[i] = a[i];
        a =(T[]) new Object[capacity];
        a = array2;
    }
}
