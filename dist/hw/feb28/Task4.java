package practice.feb21;

import java.util.Scanner;

public class Task4 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String s = "(())";
        MyLinkedStack<Character> stack = new MyLinkedStack<>();
        boolean check = false;
        int i = 0;

        while(!check && i < s.length()) {
            if (s.charAt(i) == '(' || s.charAt(i) == '[' || s.charAt(i) == '{')
                stack.push(s.charAt(i));

            switch (s.charAt(i)) {

                case(')'):
                    if (stack.isEmpty() || stack.peek() != '(')
                        check = true;
                    else
                        stack.pop();
                    break;

                case('}'):
                    if (stack.isEmpty() || stack.peek() != '{')
                        check = true;
                    else
                        stack.pop();
                    break;

                case(']'):
                    if (stack.isEmpty() || stack.peek() != '[')
                        check = true;
                    else
                        stack.pop();
                    break;
            }

            i++;
        }

        if (!check && stack.isEmpty())
            System.out.println(true);
        else
            System.out.println(false);
    }
}
