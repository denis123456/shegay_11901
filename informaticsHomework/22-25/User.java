package streamAPI;

import java.util.LinkedList;

public class User {

    private String name;
    private String city;
    private int year;
    private int id;
    private Subscribers subscribers;

    public User(String name, String city, int year) {

        this.name = name;
        this.city = city;
        this.year = year;
        subscribers = new Subscribers();
    }

    public String getName() {

        return this.name;
    }

    public String getCity() {

        return this.city;
    }

    public int getAge() {

        return 2020 - this.year;
    }

    public String toString() {

        return this.getName();
    }

    public LinkedList<User> getSubscribers() {

        return subscribers.getSubs();
    }

    public void subscribe(User to) {

        to.getSubscribers().add(this);
    }
}
