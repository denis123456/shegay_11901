package tasks;

import java.util.Scanner;
import java.util.Stack;

public class ReversePolish {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String[] s = in.nextLine().split(" ");
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < s.length; i++) {
            if (!s[i].equals("*") && !s[i].equals("+") && !s[i].equals("-") && !s[i].equals("/"))
                stack.push(Integer.parseInt(s[i]));
            else {
                int x = stack.pop();
                int y = stack.pop();
                switch (s[i]) {

                    case "*" : stack.push(x * y);
                    break;

                    case "-" : stack.push(y - x);
                    break;

                    case "+" : stack.push(x + y);
                    break;

                    case "/" : stack.push(y / x);
                    break;
                }
            }
        }
        System.out.println(stack.pop());
    }
}
