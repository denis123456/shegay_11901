/**
* @author Denis Shegay
* 11-901
* Task13
*/
import java.util.*;
import java.io.*;

public class HTask13{
	
	public static void main(String args[]){
		
		Scanner in = new Scanner(System.in);
		double x = in.nextDouble();
		double y = in.nextDouble();
		double z = in.nextDouble();
		double u = ((x + y + 2.1)*z/(x - z - 5.6) + y) * y + (z + 1.1)/(z - 2.0);
		System.out.print(u);
	}
}