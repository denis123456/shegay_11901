package tasks;

import myCollections.MyArrayStack;

import java.util.Scanner;

public class B_BracketsBalance {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        System.out.println(solve(s));
    }

    public static boolean solve(String s) {

        MyArrayStack<Character> stack = new MyArrayStack<>();
        char current;

        for (int i = 0; i < s.length(); i++) {
            current = s.charAt(i);
            if (current == '(')
                stack.push(current);
            else if (!stack.isEmpty() && stack.peek().equals(current))
                stack.pop();
            else
                return false;
        }

        return true;
    }
}
