import java.util.*;

public class HTask22 {
	
	static final int N = 26;
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		int[] a = new int[N];
		a = count(a, s);
		output(a);
	}
	
	public static void output(int[] a) {
		
		for (int i = 0; i < N; i++)
			System.out.println((char)(i + (int)'a') + " " + a[i]);
	}
	
	public static int[] count(int[] a, String s) {
		
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) >= 'a' && s.charAt(i) <= 'z')
				a[s.charAt(i) - 'a']++;
			if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')
				a[s.charAt(i) - 'A']++;
		}
		
		return a;
	}	
}