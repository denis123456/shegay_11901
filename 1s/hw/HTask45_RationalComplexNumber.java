public class HTask45_RationalComplexNumber {

	private RationalFraction a, b;
	
	public RationalComplexNumber() {
		
		this(new RationalFraction(), new RationalFraction());
	}
	
	public RationalComplexNumber(RationalFraction a, RationalFraction b) {
		
		this.a = a;
		this.b = b;
	}
	
	public RationalComplexNumber add(RationalComplexNumber number) {
		
		RationalComplexNumber newNumber =
		new RationalComplexNumber(this.a.add(number.a), this.b.add(number.b));
		
		return newNumber;
	}
	
	public RationalComplexNumber sub(RationalComplexNumber number) {
		
		RationalComplexNumber newNumber = 
		new RationalComplexNumber(this.a.sub(number.a), this.b.sub(number.b));
		
		return newNumber;
	}
	
	public RationalComplexNumber mult(RationalComplexNumber number) {
		
		RationalComplexNumber newNumber = 
		new RationalComplexNumber(this.a.mult(number.a).sub(this.b.mult(number.b)), 
						this.a.mult(number.b).add(this.b.mult(number.a)));
	
		return newNumber;
	}
	
	public RationalFraction getA() {
		
		return this.a;
	}
	
	public RationalFraction getB() {
		
		return this.b;
	}
	
	public void setA(RationalFraction a) {

		this.a = a;
	}

	public void setB(RationalFraction b) {

		this.b = b;
	}

	public String toString() {
		
		boolean flag = false;
		
		String s = "";
		
		if (this.a.value() >= 0) {
			flag = true;
			s += "" + this.a + " ";
		}
		
		if (this.a.value() < 0) {
			flag = true;
			this.a.setA(Math.abs(this.a.getA()));
			this.b.setB(Math.abs(this.a.getB()));
			s += "-" + this.a + " ";
		}
		
		if (this.b.value() < 0) {
			this.b.setA(Math.abs(this.b.getA()));
			this.b.setB(Math.abs(this.b.getB()));
			s += "- " + this.b + "i";
		} 
		if (this.b.value() > 0) {
			if (!flag)
				s += "" + this.b + "i";
			else 
				s += "+ " + this.b + "i";
		}
		if (this.b.value() == 0)
			s += "0";
		
		if (s.length() < 1)
			s = "0";

		return s;
	}
}