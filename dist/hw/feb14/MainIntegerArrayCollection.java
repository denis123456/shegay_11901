import java.util.Collection;

public class MainIntegerArrayCollection {

    public static void main(String[] args) {

        IntegerArrayCollection a = new IntegerArrayCollection();
        IntegerArrayCollection b = new IntegerArrayCollection();
        for (int i = 0; i < 10; i++)
            a.add(i);
        for (int i = 10; i > 0; i--)
            b.add(i);
        System.out.println(a.size());
        System.out.println(a.isEmpty());
        System.out.println(a.contains(0));
        System.out.println(a.toArray());
        System.out.println(a.add(10));
        System.out.println(a.remove(1));
        System.out.println(a.containsAll(b));
        System.out.println(a.removeAll(b));
        System.out.println(a.retainAll(b));
        a.clear();
        System.out.println(a.size());
    }
}
