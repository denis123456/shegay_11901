/**
* @author Denis Shegay
* 11-901
* Task 01
*/
import java.util.*;
import java.io.*;

public class HTask01{
	
	public static void main(String args[]){
	
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		x *= x*x*x*x;
		x *= x*x*x*x;
		x *= x;
		System.out.print(x);
	}
}