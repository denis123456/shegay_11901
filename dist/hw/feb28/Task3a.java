package practice.feb21;

public class Task3a {

    public static void main(String[] args) {

        String s = "(()))";
        int ans = 0;
        int i = 0;

        while (ans >= 0 && i < s.length()) {
            if (s.charAt(i) == '(')
                ans++;
            if (s.charAt(i) == ')')
                ans--;
            i++;
        }

        System.out.println(ans == 0);
    }
}
