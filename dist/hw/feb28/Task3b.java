package practice.feb21;

public class Task3b {

    public static void main(String[] args) {

        String s = "(())";
        MyLinkedStack<Character> stack = new MyLinkedStack<Character>();
        boolean check = false;
        int i = 0;

        while (!check && i < s.length()) {
            if (s.charAt(i) == '(')
                stack.push(s.charAt(i));
            if (s.charAt(i) == ')')
                stack.pop();
            if ((stack.isEmpty() || stack.peek() != '(') && s.charAt(i) == ')')
                check = true;
            i++;
        }
        if (!check && stack.isEmpty())
            System.out.println(true);
        else
            System.out.println(false);
    }
}
