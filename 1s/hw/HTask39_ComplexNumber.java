public class HTask39_ComplexNumber {
	
	private double a, b;
	
	public ComplexNumber() {
		
		this(0, 0);
	}
	
	public ComplexNumber(double a, double b) {
		
		this.a = a;
		this.b = b;
	}
	
	public ComplexNumber add(ComplexNumber cNumber) {
		
		ComplexNumber newCNumber = 
		new ComplexNumber(this.a + cNumber.a, this.b + cNumber.b);
		
		return newCNumber;
	}
	
	public ComplexNumber sub(ComplexNumber cNumber) {
		
		ComplexNumber newCNumber = 
		new ComplexNumber(this.a - cNumber.a, this.b - cNumber.b);
		
		return newCNumber;
	}
	
	public ComplexNumber mult(ComplexNumber cNumber) {
		
		ComplexNumber newCNumber = 
		new ComplexNumber(this.a * cNumber.a - this.b * cNumber.b, 
						this.a * cNumber.b + this.b * cNumber.a);
	
		return newCNumber;
	}
	
	public ComplexNumber div(ComplexNumber cNumber) {
		
		double x = cNumber.a * cNumber.a - cNumber.b * cNumber.b;
		cNumber.b *= -1;
		ComplexNumber newCNumber = 
		new ComplexNumber((this.a * cNumber.a - this.b * cNumber.b) / x, 
						(this.a * cNumber.b + this.b * cNumber.a) / x);
		
		return newCNumber;
	}
	
	public void add2(ComplexNumber cNumber) {
		
		this.a += cNumber.a;
		this.b += cNumber.b;
	}
	
	public void sub2(ComplexNumber cNumber) {
		
		this.a -= cNumber.a;
		this.b -= cNumber.b;
	}
	
	public void mult2(ComplexNumber cNumber) {
		
		double x = this.a;
		this.a = this.a * cNumber.a - this.b * cNumber.b;
		this.b = x * cNumber.b + this.b * cNumber.a;
	}
	
	public void div2(ComplexNumber cNumber) {
		
		double x = cNumber.a * cNumber.a - cNumber.b * cNumber.b;
		double a = this.a;
		cNumber.b *= -1;
		this.a = (this.a * cNumber.a - this.b * cNumber.b) / x;
		this.b = (a * cNumber.b + this.b * cNumber.a) / x;
	}
		
	public double length() {
		
		return Math.sqrt(this.a * this.a + this.b * this.b);
	}
	
	public String toString() {
		
		String s = "";
		if (this.a < 0)
			s += "" + "-" + Math.abs(a) + " ";
		if (this.a > 0)
			s += "" + Math.abs(a) + " ";
		if (s != "" && b != 0) {
			if (this.b < 0)
				s += "" + "-" + Math.abs(b) + "i";
			if (this.b > 0)
				s += "" + "+" + Math.abs(b) + "i";
		}
		if (s == "" && b != 0) {
			if (b > 0)
				s += "" + Math.abs(b) + "i";
			if (b < 0)
				s += "" + "-" + Math.abs(b) + "i";
		}
		
		if (s.length() < 1)
			s = "0";
		
		return s;
	}
	
	public double getA() {
		
		return this.a;
	}
	
	public double getB() {
		
		return this.b;
	}
	
	public void setA(double a) {
		
		this.a = a;
	}
	
	public void setB(double b) {
		
		this.b = b;
	}
	
	public double arg() {
		
		return Math.tan(this.b / this.a);
	}
	
	public ComplexNumber pow(double n) {
		
		double arg = this.arg() * n;
		double z = Math.pow(this.length(), n);
		ComplexNumber newCNumber = 
		new ComplexNumber(z * Math.cos(arg), z * Math.sin(arg));
		
		return newCNumber;
	}
	
	public boolean equals(ComplexNumber cNumber) {
		 
		if (this.a == cNumber.a && this.b == cNumber.b)
			return true;
		else
			return false;
	}
}