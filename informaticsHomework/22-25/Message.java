package streamAPI;

import java.util.Date;

public class Message {

    private User sender;
    private User receiver;
    private boolean status;
    private Date date;
    private String text;

    public Message(User sender, User receiver, String text) {

        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
    }

    public User getSender() {

        return this.sender;
    }

    public boolean getStatus() {

        return status;
    }

    public User getReceiver() {

        return this.receiver;
    }

}
