package filmTask;

public class Producer {

    private String name;
    private int id;

    public Producer(String name, int id) {

        this.name = name;
        this.id = id;
    }

    public int getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public String toString() {

        return name;
    }
}
