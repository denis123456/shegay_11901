/**
* CW variant 1
* @author Denis Shegay
* 11-901
* Task 01
*/

import java.util.*;

public class Task01 {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int n = Integer.parseInt(args[0]);
		String[] s = new String[n];
		for (int i = 0; i < n - 1; i++) {
			s[i] = args[i + 1];
			if (isThree(s[i]))
				s[i] = replace(s[i]);
		}
		for (int i = 0; i < n; i++)
			System.out.println(s[i]);
	}

	public static boolean isThree (String s) {

		int count = 0;
		int i = 0;
		char a;
		while (count < 3 && i < s.length()) {
			a = s.charAt(i);
			if (a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u' || a == 'y' || a == 'A' || a == 'I' || a == 'O' || a == 'U' || a == 'Y')
				count++;
			i++;
		}
		if (count >= 3)
			return true;
		else
			return false;
	}

	public static String replace(String s) {

		String ans = "";
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')
				ans += "z";
			else
				ans += s.charAt(i);
		return ans;
	} 
}