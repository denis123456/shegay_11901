public class HTask43_RationalMatrix2x2 {

	private RationalFraction[][] a = new RationalFraction[2][2];

	public RationalMatrix2x2() {
		
		this(new RationalFraction(),
			 new RationalFraction(),
			 new RationalFraction(),
			 new RationalFraction());
	}
	
	public RationalMatrix2x2(RationalFraction a) {
		
		this.a[0][0] = a;
		this.a[0][1] = a;
		this.a[1][0] = a;
		this.a[1][1] = a;
	}
	
	public RationalMatrix2x2(RationalFraction a, RationalFraction b,
	RationalFraction c, RationalFraction d) {
		
		this.a[0][0] = a;
		this.a[0][1] = b;
		this.a[1][0] = c;
		this.a[1][1] = d;
	}
	
	public RationalMatrix2x2 add(RationalMatrix2x2 matrix) {
		
		RationalMatrix2x2 newMatrix = new RationalMatrix2x2();
		
		newMatrix.a[0][0] = this.a[0][0].add(matrix.a[0][0]);
		newMatrix.a[0][1] = this.a[0][1].add(matrix.a[0][1]);
		newMatrix.a[1][0] = this.a[1][0].add(matrix.a[1][0]);
		newMatrix.a[1][1] = this.a[1][1].add(matrix.a[1][1]);
		
		return newMatrix;
	}
	
	public RationalMatrix2x2 mult(RationalMatrix2x2 matrix) {
		
		RationalMatrix2x2 newMatrix = new RationalMatrix2x2();
		
		newMatrix.a[0][0] = 
		this.a[0][0].mult(matrix.a[0][0]).add(this.a[0][1].mult(matrix.a[1][0]));
		newMatrix.a[0][1] = 
		this.a[0][0].mult(matrix.a[0][1]).add(this.a[0][1].mult(matrix.a[1][1]));
		newMatrix.a[1][0] = 
		this.a[1][0].mult(matrix.a[0][0]).add(this.a[0][1].mult(matrix.a[1][0]));
		newMatrix.a[1][1] = 
		this.a[1][0].mult(matrix.a[0][1]).add(this.a[1][1].mult(matrix.a[1][1]));
		
		return newMatrix;
	}
	
	public RationalFraction det() {
		
		return (this.a[0][0].mult(this.a[1][1])).sub(this.a[0][1].mult(this.a[1][0]));
	}
	
	public RationalVector2D multVector(RationalVector2D vector) {
		
		RationalVector2D newVector = new RationalVector2D();
		
		newVector.setA(this.a[0][0].mult(vector.getA()).add(this.a[0][1].mult(vector.getA())));
		newVector.setB(this.a[0][0].mult(vector.getB()).add(this.a[0][1].mult(vector.getB())));
		
		return newVector;
	}
	
	public String toString() {
		
		return "" + this.a[0][0] + " " + this.a[0][1] + "\n" 
				+ this.a[1][0] + " " + this.a[1][1];
	}
}