package filmTask;

public class Actor {

    private String name;
    private int id;
    private int exp;

    public Actor(String name, int id, int exp) {

        this.name = name;
        this.id = id;
        this.exp = exp;
    }

    public String getName() {

        return name;
    }

    public int getId() {

        return id;
    }

    public int getExp() {

        return exp;
    }

    public String toString() {

        return name;
    }
}
