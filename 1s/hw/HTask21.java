/**
* @author Denis Shegay
* 11-901
* Task 21
*/

import java.util.*;

public class HTask21 {
	
	public static void main(String args[]) {
		
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		System.out.print(count(s));
	}
	
	public static int count (String s) {
		
		int ans = 0;
		for (int i = 0; i < s.length() - 1; i++) 
			if (s.charAt(i) == ' ' && 'A' <= s.charAt(i + 1) && s.charAt(i + 1) <= 'Z')
				ans++;
		if 	('A' <= s.charAt(s.length() - 1) && s.charAt(s.length() - 1) <= 'Z')
			ans++;
		if ('A' <= s.charAt(0) && s.charAt(0) <= 'Z')
			ans++;
		return ans;
	}
}