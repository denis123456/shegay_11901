public class HTask38_RationalFraction {

	private int a, b;
	
	public RationalFraction() {
		
		this(0, 1);
	}
	
	public RationalFraction(int a, int b) {
		
		this.a = a;
		this.b = b;
	}
	
	public void reduce() {
		
		int x = 0;
		
		while (nod(this.a, this.b) > 1) {
		
			x = nod(a, b);
			this.a /= x;
			this.b /= x;
		}
	}
	
	private int nod(int a, int b) {
		
		while(a != 0 && b != 0) {
		
			if (a > b)
				a = a % b;
			else
				b = b % a;
		}
		
		return a + b;
	}
	
	public RationalFraction add(RationalFraction fraction) {

		int div = nok(this.b, fraction.b);
		int a1 = this.a * div / fraction.b;
		int a2 = fraction.a * div / this.b;
		RationalFraction ans = new RationalFraction(a1 + a2, div);		
		ans.reduce();
		
		return ans;
	}	
		
	private int nok(int m, int n) {
		
		return m * n/nod(m, n);
	}
		
	public void add2(RationalFraction fraction) {
		
		int k = nok(this.b, fraction.b);
		this.a = this.a * k / this.b + fraction.a * k / fraction.b;
		this.b = k;
		this.reduce();
	}
	
	public RationalFraction sub(RationalFraction fraction) {
		
		int div = nok(this.b, fraction.b);
		int a1 = this.a * div / fraction.b;
		int a2 = fraction.a * div / this.b;
		RationalFraction ans = new RationalFraction(a1 - a2, div);		
		ans.reduce();

		return ans;
	}
	
	public void sub2(RationalFraction fraction) {
		
		int k = nok(this.b, fraction.b);
		this.a = this.a * k / this.b - fraction.a * k / fraction.b;
		this.b = k;
		this.reduce();
	}
	
	public RationalFraction mult(RationalFraction fraction) {
		
		RationalFraction ans = new RationalFraction
							(a * fraction.a, this.b * fraction.b);
		ans.reduce();
		
		return ans;
	}
	
	public void mult2(RationalFraction fraction) {
		
		this.a *= fraction.a;
		this.b *= fraction.b;
		this.reduce();
	}
	
	public RationalFraction div(RationalFraction fraction) {
		
		RationalFraction ans = new RationalFraction
							(this.a * fraction.b, this.b * fraction.a);
		ans.reduce();
		
		return ans;
	}
	
	public void div2(RationalFraction fraction) {
		
		this.a *= fraction.b;
		this.b *= fraction.a;
		this.reduce();
	}
	
	public String toString() {
		
		if (this.a != 0) {
			if (this.b != 1)
				return "" + this.a + "/" + this.b;
			else
				return "" + this.a;
		} else
			return "0";
	}
		
	public double value() {
		
		return (double)this.a/this.b;
	}
		
	public boolean equals(RationalFraction fraction) {
		
		if (this.value() == fraction.value())
			return true;
		else
			return false;
	}
	
	public int getA() {
		
		return this.a;
	}
	
	public int getB() {
		
		return this.b;
	}
	
	public void setA(int a) {
		
		this.a = a;
	}
	
	public void setB(int b) {
		
		this.b = b;
	}
	
	public int numberPart() {
		
		return this.a/this.b;
	}
}