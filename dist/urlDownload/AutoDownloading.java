package urlDownload;

import java.io.*;
import java.net.URL;
import java.util.Scanner;

public class AutoDownloading {

    public static void main(String[] args) throws IOException {

        URL url = new URL("https://vk.com/myenghumor?w=wall-91674513_1421478");
        InputStream in = new BufferedInputStream(url.openStream());
        OutputStream out = new BufferedOutputStream(new FileOutputStream("file.txt"));
        File file = new File("file.txt");
        int i = in.read();
        while (i >= 0) {
            out.write(i);
            i = in.read();
        }
        in.close();
        out.close();

        Scanner scan = new Scanner(file);
        String line;
        String[] links;
        int counter = 1;
        while (scan.hasNextLine() && counter <= 15) {
            line = scan.nextLine();
            links = line.split("\"");
            for (i = 0; i < links.length; i++) {
                if (links[i].length() > 3 && links[i].charAt(0) == 'h' && links[i].charAt(1) == 't' && !links[i].contains("https host = null")) {
                    URL url2 = new URL(links[i]);
                    try {
                        in = new BufferedInputStream(url2.openStream());
                        out = new BufferedOutputStream(new FileOutputStream("picture" + counter + ".png"));
                        int k = in.read();
                        while (k >= 0) {
                            out.write(k);
                            k = in.read();
                        }
                        in.close();
                        out.close();
                        counter++;
                    } catch (FileNotFoundException | IllegalArgumentException ignored) {}
                }
            }
        }
    }
}
