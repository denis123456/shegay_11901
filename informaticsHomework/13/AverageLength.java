package tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class AverageLength {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("C:\\Users\\densh\\IdeaProjects\\classwork\\src\\strings");
        Scanner in = new Scanner(file);
        List<String> list = new LinkedList<>();

        while (in.hasNextLine())
            list.add(in.nextLine());

        System.out.println(list.stream().map(String::length).mapToDouble(a -> a).sum()/list.size());
    }
}
