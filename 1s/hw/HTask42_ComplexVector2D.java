public class HTask42_ComplexVector2D {

	private ComplexNumber a, b;
	
	public ComplexVector2D() {
		
		this(new ComplexNumber(),new ComplexNumber());
	}
	
	public ComplexVector2D(ComplexNumber a, ComplexNumber b) {
		
		this.a = a;
		this.b = b;
	}
	
	public ComplexVector2D add(ComplexVector2D vector) {
		
		ComplexVector2D newVector = 
		new ComplexVector2D(this.a.add(vector.a), this.b.add(vector.b));
	
		return newVector;
	}
	
	public boolean equals(ComplexVector2D vector) {
		
		if (this.a == vector.a && this.b == vector.b)
			return true;
		else
			return false;
	}
	
	public ComplexNumber getA() {
		
		return this.a;
	}
	
	public ComplexNumber getB() {
		
		return this.b;
	}
	
	public void setA(ComplexNumber a) {
		
		this.a = a;
	}
	
	public void setB(ComplexNumber b) {
		
		this.b = a;
	}
	
	public String toString() {
		
		return "{" + this.a.toString() + ", " + this.b.toString() + "}";
	}
	
	public ComplexNumber scalarProduct(ComplexVector2D vector) {
		
		ComplexNumber newCNumber = new ComplexNumber();
		newCNumber.setA((this.a.mult(vector.a)).getA());
		newCNumber.setB((this.a.mult(vector.a)).getB());
		return newCNumber;
	}
}