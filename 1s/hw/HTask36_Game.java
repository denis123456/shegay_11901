import java.util.*;

public class HTask36_Game {
 
	public static void main(String[] args) {
		
		(new Game()).run();
	}
	 
	public void run() {
		
		Scanner in = new Scanner(System.in);
		String name = "";
		int i = 0;
		int damage = 0;
		Player[] p = new Player[2];
		String dmg = "";
		boolean check = false;
		
		for (i = 0; i < 2; i++) {
			System.out.print("Player " + (i + 1) + " name: ");
			name = in.nextLine();
			System.out.println();
			p[i] = new Player(name);
		}
		
		i = 1;
		while (p[1].hp != 0 && p[0].hp != 0) {
			i = (i + 1) % 2;
			System.out.println(p[i].name + ", get ready to attack!");
			System.out.print("Enter damage: ");
			check = false;
			
			while (!check) {
				dmg = in.nextLine();
				System.out.println();
				
				if (dmg.charAt(0) > '0' && dmg.charAt(0) <= '9' && dmg.length() == 1) {
					check = true;
					damage = Integer.parseInt(dmg);
				}
				
				if (!check) {
					System.out.println("Error: Incorrect input");
					System.out.print("Enter damage: ");
				}
			}
			
			p[i].attack(p[(i + 1) % 2], damage);
			System.out.println(p[0].name + " has " + p[0].hp + "hp");
			System.out.println(p[1].name + " has " + p[1].hp + "hp");			
			System.out.println();
		}
	}
}
