package iostream;

import java.io.*;
import java.util.Random;

public class IOTask3 {

    public static void main(String[] args) throws IOException {

        Random random = new Random();
        byte[] bytes = new byte[3 + random.nextInt(4)];
        InputStream is = new FileInputStream("out2.txt");
        OutputStream os = new FileOutputStream("out3.txt");
        while (is.read() > 0) {
            is.read(bytes);
            os.write(bytes);
        }
    }
}
