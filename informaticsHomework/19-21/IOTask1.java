package iostream;

import java.io.*;
import java.util.Random;

public class IOTask1 {

    public static void main(String[] args) throws IOException {

        Writer writer = new OutputStreamWriter(new FileOutputStream("1.txt"));
        Random random = new Random();

        int n = 10 + random.nextInt(10);
        for (int i = 0; i < n; i++) {
            int x = random.nextInt(100);
            writer.write(x);
        }
        writer.close();
    }
}
