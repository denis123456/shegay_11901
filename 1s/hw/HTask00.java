/**
* @author Denis Shegay
* 11-901
* Task 00
*/
import java.util.*;
import java.io.*;

public class HTask00{
	
	public static void main(String args[]){
	
		System.out.println(" 111   11111  1   1  1   1  11111");
		System.out.println(" 1 1   1      1   1  1  11  1");
		System.out.println(" 1 1   11111  11111  1 1 1  1");
		System.out.println("11111  1      1   1  11  1  1");
		System.out.println("1   1  11111  1   1  1   1  11111");
	}
}