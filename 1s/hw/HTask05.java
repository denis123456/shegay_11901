/**
* @author Denis Shegay
* 11-901
* Task 05
*/
import java.util.*;
import java.io.*;

public class HTask05{

	public static void main(String args[]){
	
	 	Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int i = 0; i < n; i++){
			for (int j = n - i - 1; j > 0; j--)
				System.out.print(" ");
			for (int k = 0; k < n; k++)
				System.out.print(1);
			System.out.println();
		}
	}
}
	