/**
* @author Denis Shegay
* 11-901
* Task 04
*/
import java.util.*;
import java.io.*;

public class HTask04{

	public static void main(String args[]){
	
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++)
				System.out.print(1);
			System.out.println();
		}
	}
}