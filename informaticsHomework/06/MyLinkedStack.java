package myCollections;

import myCollections.MyLinkedList.Node;

public class MyLinkedStack {

    private Node last;
    private int size;

    public MyLinkedStack() {

        size = 0;
        last = new Node();
    }

    public void push(Object o) {

        Node node = new Node((int) o);
        node.next = last;
        size++;
        last = node;
    }

    public int pop() {

        Node ans = last;
        last = last.next;
        size--;

        return ans.getVal();
    }

    public boolean isEmpty() {

        return size == 0;
    }

    public int peek() {

        return last.getVal();
    }

    public int getSize() {

        return size;
    }
}
