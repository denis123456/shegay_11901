package controlWork;

public class Airport {

    private String city;
    private String code;
    private String country;
    private String abbreviation;

    public Airport(String city, String code, String country, String abbreviation) {

        this.city = city;
        this.code = code;
        this.country = country;
        this.abbreviation = abbreviation;
    }
}
