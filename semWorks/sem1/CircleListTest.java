package sem1;

import org.junit.Assert;
import org.junit.Test;

public class CircleListTest {

    @Test
    public void searchMethodCheck() {

        CircleList list = new CircleList();
        Participant p1 = new Participant("Jack", "male");
        Participant p2 = new Participant("Tom", "male");
        Participant p3 = new Participant("Emma", "female");
        list.insert(p1);
        list.insert(p2);
        list.insert(p3);
        Assert.assertEquals(list.search("Jack"), p1);
    }

    @Test
    public void genderMethodCheck() {
        CircleList list = new CircleList();
        Participant p1 = new Participant("Jack", "male");
        Participant p2 = new Participant("Tom", "male");
        Participant p3 = new Participant("Emma", "female");
        list.insert(p1);
        list.insert(p2);
        list.insert(p3);
        CircleList[] a = list.gender();
        Assert.assertEquals(a[0].head.getValue().getName(), "Jack");
    }
}
