/**
* CW variant 2
* @author Denis Shegay 
* 11-901
* Task 02
*/

import java.util.*;
import java.io.*;

public class Task02 {
	
	public static void main(String args[]) {
		
		Scanner in = new Scanner(System.in);
		double x = in.nextDouble();
		int n0 = in.nextInt();
		int n = 1;
		double a0 = (x + n0)*(x + n0);
		double a = 1;
		int k = 0;
		int f = 1;
		double s = 0;
		for (int i = 1; i <= n0; i++) {
			n *= n0;
			f *= ++k;
			a *= a0;
			s += (n + k)/a;
		}
		System.out.print(s);
	}
}