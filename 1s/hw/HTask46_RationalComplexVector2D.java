public class HTask46_RationalComplexVector2D {

	private RationalComplexNumber a, b;
	
	public RationalComplexVector2D() {
		
		this(new RationalComplexNumber(), new RationalComplexNumber());
	}
	
	public RationalComplexVector2D(RationalComplexNumber a, RationalComplexNumber b) {
		
		this.a = a;
		this.b = b;
	}
	
	public RationalComplexVector2D add(RationalComplexVector2D vector) {
		
		RationalComplexVector2D newVector = 
		new RationalComplexVector2D(this.a.add(vector.a), this.b.add(vector.b));
	
		return newVector;
	}
	
	public String toString() {
		
		String s = "" + "{" + this.a + ", " + this.b + "}";
		
		return s;
	}
	
	public RationalComplexNumber getA() {

		return this.a;
	}

	public RationalComplexNumber getB() {

		return this.b;
	}

	public void setA(RationalComplexNumber a) {

		this.a = a;
	}

	public void setB(RationalComplexNumber b) {

		this.b = b;
	}

	public RationalComplexNumber scalarProduct(RationalComplexVector2D vector) {
		
		RationalComplexNumber newNumber = new RationalComplexNumber();
		
		newNumber.setA(((this.a.mult(vector.a)).getA()).add(this.b.mult(vector.b).getA()));
		newNumber.setB(((this.b.mult(vector.b)).getB()).add(this.b.mult(vector.b).getB()));

		return newNumber;
	}
}