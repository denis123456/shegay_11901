/**
* @author Denis Shegay
* 11-901
* Task 03
*/
import java.util.*;
import java.io.*;

public class HTask03{
	
	public static void main(String args[]){
	 
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		int s = 0;
		while (x != 0){
			s += x % 10;
			x /= 10;
		}
		System.out.print(s);
	}
}