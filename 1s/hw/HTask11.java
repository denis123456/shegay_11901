/**
* @author Denis Shegay
* 11-901
* Task 11
*/
import java.util.*;
import java.io.*;

public class HTask11{

	public static void main(String args[]){
	
		Scanner in = new Scanner(System.in);
		double R = 5.0;
		double y1 = R * 0.5; 
		double y2 = y1*(-1);
		double x = in.nextDouble(); 
		double y = in.nextDouble();
		if (Math.sqrt(x*x + (y-y1)*(y-y1)) <= y1 && y >= 0)
			System.out.print("In");
		else
		if (Math.sqrt(x*x + y*y) <= R && x >= 0)
			if (y <= 0 && Math.sqrt(x*x + (y-y1)*(y-y1)) > y1)
				System.out.print("Out");
			else
				System.out.print("In");
		else
			System.out.print("Out");
	}
}