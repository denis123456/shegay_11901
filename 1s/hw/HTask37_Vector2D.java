public class HTask37_Vector2D {

	private double a, b;
	
	public Vector2D() {
		
		this(0, 0);
	}
	
	public Vector2D(double a, double b) {
		
		this.a = a;
		this.b = b;
	}
	
	public Vector2D add (Vector2D vector) {
		
		Vector2D newVector = new Vector2D(this.a + vector.a, this.b + vector.b);
	
		return newVector;
	}
	
	public Vector2D sub(Vector2D vector) {
		
		Vector2D newVector = new Vector2D(this.a - vector.a, this.b - vector.b);
		
		return newVector;
	}
	
	public Vector2D mult(double k) {
		
		Vector2D newVector = new Vector2D(this.a * k, this.b * k);
		
		return newVector;
	}
	
	public void add2(Vector2D vector) {
		
		this.a += vector.a;
		this.b += vector.b;
	}
	
	public void sub2(Vector2D vector) {
		
		this.a -= vector.a;
		this.b -= vector.b;
	}
	
	public void mult2(double k) {
		
		this.a *= k;
		this.b *= k;
	}
	
	public String toString() {
		
		String s = "" + "{" + this.a + "; " + this.b + "}";
		
		return s;
	}
	
	public double getA() {
		
		return this.a;
	}
	
	public void setA(double a) {
		
		this.a = a;
	}
	
	public void setB(double b) {
		
		this.b = b;
	}
	
	public double getB() {
		
		return this.b;
	}
	
	public double length() {
		
		return Math.sqrt(a * a + b * b);
	}
	
	public double scalarProduct(Vector2D vector) {
		
		return this.length() * vector.length() * this.cos(vector);
	}
	
	public double cos(Vector2D vector) {
		
		return (this.a * vector.a + this.b * vector.b) / (this.length() * vector.length());
	}
	
	public boolean equals (Vector2D vector) {
		
		if (this.a == vector.a && this.b == vector.b)
			return true;
		else
			return false;
	}
}