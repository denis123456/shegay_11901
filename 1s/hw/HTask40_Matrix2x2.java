public class HTask40_Matrix2x2 {

	private double[][] a = new double[2][2];
	
	public Matrix2x2() {
		
		this(0, 0, 0, 0);
	}
	
	public Matrix2x2(double a) {
		
		this(a, a, a, a);
	}
	
	public Matrix2x2(double a, double b, double c, double d) {
		
		this.a[0][0] = a;
		this.a[0][1] = b;
		this.a[1][0] = c;
		this.a[1][1] = d;
	}
	
	public Matrix2x2 add(Matrix2x2 matrix) {
		
		Matrix2x2 newMatrix = 
		new Matrix2x2(this.a[0][0] + matrix.a[0][0], this.a[0][1] + matrix.a[0][1], 
						this.a[1][0] + matrix.a[1][0], this.a[1][1] + matrix.a[1][1]);
						
		return newMatrix;
	}
	
	public Matrix2x2 sub(Matrix2x2 matrix) {
		
		Matrix2x2 newMatrix = 
		new Matrix2x2(this.a[0][0] - matrix.a[0][0], this.a[0][1] - matrix.a[0][1], 
					this.a[1][0] - matrix.a[1][0], this.a[1][1] - matrix.a[1][1]);
						
		return newMatrix;
	}
	
	public Matrix2x2 multNumber(double x) {
		
		Matrix2x2 newMatrix = new Matrix2x2(this.a[0][0] * x, this.a[0][1] * x,
										this.a[1][0] * x, this.a[1][1] * x);
		
		return newMatrix;
	}
	
	public Matrix2x2 mult(Matrix2x2 matrix) {
		
		Matrix2x2 newMatrix = new Matrix2x2();
		
		newMatrix.a[0][0] = this.a[0][0] * matrix.a[0][0] + this. a[0][1] * matrix.a[1][0];
		newMatrix.a[0][1] = this.a[0][0] * matrix.a[0][1] + this.a[0][1] * matrix.a[1][1];
		newMatrix.a[1][0] = this.a[1][0] * matrix.a[0][0] + this.a[1][1] * matrix.a[1][0];
		newMatrix.a[1][1] = this.a[1][0] * matrix.a[0][1] + this.a[0][1] * matrix.a[1][1];
		
		return newMatrix;
	}
	
	public void add2(Matrix2x2 matrix) {
		
		for (int i = 0; i < 2; i++) 
			for (int j = 0; j < 2; j++)
				this.a[i][j] += matrix.a[i][j];
	}
	
	public void sub2(Matrix2x2 matrix) {
		
		for (int i = 0; i < 2; i++) 
			for (int j = 0; j < 2; j++)
				this.a[i][j] -= matrix.a[i][j];
	}
	
	public void multNumber2(double x) {
		
		for (int i = 0; i < 2; i++) 
			for (int j = 0; j < 2; j++)
				this.a[i][j] *= x;
	}
	
	public void mult2(Matrix2x2 matrix) {
		
		Matrix2x2 newMatrix = new Matrix2x2();
		
		double a = this.a[0][0];
		double b = this.a[0][1];
		double c = this.a[1][0];
		double d = this.a[1][1];
		
		this.a[0][0] = a * matrix.a[0][0] + c * matrix.a[1][0];
		this.a[0][1] = a * matrix.a[0][1] + b * matrix.a[1][1];
		this.a[1][0] = c * matrix.a[0][0] + d * matrix.a[1][0];
		this.a[1][1] = c * matrix.a[0][1] + d * matrix.a[1][1];
		
	}
	
	public double det() {
		
		return this.a[0][0] * a[1][1] - a[0][1] * a[1][0];
	}
	
	public void transpon() {
		
		double x = this.a[1][0];
		this.a[1][0] = this.a[0][1];
		this.a[0][1] = x;
	}
		
	public String toString() {
		
		return "" + this.a[0][0] + " " + this.a[0][1] + "\n" 
				+ this.a[1][0] + " " + this.a[1][1];
	}
	
	public Matrix2x2 inverseMatrix() {
		
		Matrix2x2 newMatrix = new Matrix2x2();
		
		double det = this.det();
		
		if (det != 0) {
			newMatrix.a[0][0] = this.a[1][1];
			newMatrix.a[1][0] = (-1) * this.a[0][1];
			newMatrix.a[0][1] = (-1) * this.a[1][0];
			newMatrix.a[1][1] = this.a[0][0];
		
			newMatrix.transpon();
		
			newMatrix.multNumber2(1/det);
		}
		
		return newMatrix;
	}
	
	public Matrix2x2 equivalentDiagonal() { 

		Matrix2x2 newMatrix = new Matrix2x2(); 
		newMatrix.a[0][1] = 0; 
		newMatrix.a[1][1] = this.a[1][1] - this.a[1][0] * this.a[0][1] / this.a[0][0]; 
		newMatrix.a[1][0] = 0; 
		newMatrix.a[0][0] = this.a[0][0]; 
	
		return newMatrix; 
	}
	
	public Vector2D multVector(Vector2D vector) {
		
		Vector2D newVector = new Vector2D();
		
		newVector.setA(this.a[0][0] * vector.getA() + this.a[0][1] * vector.getB());
		newVector.setB(this.a[1][0] * vector.getA() + this.a[1][1] * vector.getB());
		
		return newVector;
	}
	
}