package filmTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    static List<Producer> producers = new LinkedList<>();
    static List<Actor> actors = new LinkedList<>();
    static List<Film> films = new LinkedList<>();
    static List<Participation> participations = new LinkedList<>();

    public static void main(String[] args) throws FileNotFoundException {

        String path = "C:\\Users\\densh\\IdeaProjects\\classwork\\src\\";
        File producer = new File(path + "producers");
        File actor = new File(path + "actors");
        File film = new File(path + "films");
        File participation = new File("C:\\Users\\densh\\IdeaProjects\\classwork\\src\\participations");
        readProd(producer, producers);
        readActor(actor, actors);
        readFilm(film, films);
        readPart(participation, participations);
    }

    public static List<Film> task1Stream(int before, Producer producer) {

        return films
                .stream()
                .filter(x -> x.getYear() < before)
                .filter(film -> film.getProducerId() == producer.getId()).collect(Collectors.toList());
    }

    public static List<Film> task1Default(int before, Producer producer) {

        List<Film> filteredFilms = new LinkedList<>();

        for (Film film : films)
            if (film.getProducerId() == producer.getId() && film.getYear() < before)
                filteredFilms.add(film);

        return filteredFilms;
    }

    public static int task2Stream(Actor actor, Producer producer) {

        Map<Integer, Producer> producerMap = producers.stream().collect(Collectors.toMap(Producer::getId, prod -> prod));
        Map<Integer, Actor> actorMap = actors.stream().collect(Collectors.toMap(Actor::getId, act -> act));

        //films.stream().collect(Collectors.toMap(Film::getProducerId, ))

        return 0;
    }

    public static void readProd(File file, List<Producer> list) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String[] s;
        while (in.hasNextLine()) {
            s = in.nextLine().split("\n");
            list.add(new Producer(s[0], Integer.parseInt(s[1])));
        }
    }

    public static void readActor(File file, List<Actor> list) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String[] s;
        while (in.hasNextLine()) {
            s = in.nextLine().split("\n");
            list.add(new Actor(s[0], Integer.parseInt(s[1]), Integer.parseInt(s[2])));
        }
    }

    public static void readFilm(File file, List<Film> list) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String[] s;
        while (in.hasNextLine()) {
            s = in.nextLine().split("\n");
            list.add(new Film(s[0], Integer.parseInt(s[1]), Integer.parseInt(s[2]), Integer.parseInt(s[3])));
        }
    }

    public static void readPart(File file, List<Participation> list) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String[] s;
        while (in.hasNextLine()) {
            s = in.nextLine().split("\n");
            list.add(new Participation(s[0], Integer.parseInt(s[1]), Integer.parseInt(s[2])));
        }
    }
}
