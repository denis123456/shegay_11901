package sem2;

import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        File file = new File("input.txt");
        File file1 = new File("output1.txt");
        File file2 = new File("output2.txt");

        FileWriter fileWriter = new FileWriter(file);
        FileWriter fileWriter1 = new FileWriter(file1);
        FileWriter fileWriter2 = new FileWriter(file2);

        generate(fileWriter);
        fileWriter.close();

        arraySortTest(file, fileWriter1);
        listSortTest(file, fileWriter2);
    }

    public static void arraySortTest(File file, FileWriter fileWriter) throws IOException {

        int test = 0;
        LinkedList<Integer> mistakes = new LinkedList<>();

        Scanner in = new Scanner(file);
        while (in.hasNextLine()) {
            int[] a = readAsArray(in);
            long begin = System.nanoTime();
            ATimSort.sort(a);
            long end = System.nanoTime();
            test++;
            long time = end - begin;
            if (!ATimSort.checkSorted())
                mistakes.add(test);
            String s = test + ") Time: " + time/1e+6 + " ms " +
                    "\nNumber of elements: " + a.length +
                    "\nIterations: " + ATimSort.getIterations() +
                    "\nSorted: " + ATimSort.checkSorted() + "\n";
            fileWriter.write(s);
            fileWriter.write(System.lineSeparator());
            fileWriter.write(System.lineSeparator());
        }
        fileWriter.close();
        for (Integer x : mistakes)
            System.out.print(x + " ");
    }

    public static void listSortTest(File file, FileWriter fileWriter) throws IOException {

        int test = 0;
        LinkedList<Integer> mistakes = new LinkedList<>();

        Scanner in = new Scanner(file);
        while (in.hasNextLine()) {
            LinkedList<Integer> list = readAsList(in);
            long begin = System.nanoTime();
            ListTimSort.sort(list);
            long end = System.nanoTime();
            test++;
            long time = end - begin;
            if (!ListTimSort.checkSorted())
                mistakes.add(test);
            String s = test + ") Time: " + time/1e+6 + " ms " +
                    "\nNumber of elements: " + list.size() +
                    "\nIterations: " + ListTimSort.getIterations() +
                    "\nSorted: " + ListTimSort.checkSorted() + "\n";
            fileWriter.write(s);
            fileWriter.write(System.lineSeparator());
        }
        fileWriter.close();
        for (Integer x : mistakes)
            System.out.print(x + " ");
    }

    public static int[] readAsArray(Scanner in) {

        String line = in.nextLine();
        String[] digits = line.split(" ");
        int[] arr = new int[digits.length];
        for (int i = 0; i < arr.length; i++)
            arr[i] = Integer.parseInt(digits[i]);

        return arr;
    }

    public static LinkedList<Integer> readAsList(Scanner in) {

        LinkedList<Integer> list = new LinkedList<>();
        int[] arr = readAsArray(in);
        for (int value : arr) list.add(value);

        return list;
    }

    public static void generate(FileWriter fileWriter) throws IOException {

        int n;
        int k = (int) (50 + Math.random()*50);

        for (int j = 0; j < k; j++) {
            String s = "";
            n = (int) (100 + Math.random() * 10000);
            for (int i = 0; i < n; i++)
                s += (int) (Math.random() * 100) + " ";
            fileWriter.write(s);
            fileWriter.write(System.lineSeparator());
        }
    }
}
