package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class GraphPainting {

    //-1 - no color, 0 - red, 1 - blue, 2 - green, 3 - yellow
    public static void main(String[] args) throws IOException {

        File path = new File("C:\\Users\\densh\\IdeaProjects\\classwork\\src\\graph");
        File file = new File(path,"matrix");
        boolean[][] adjacencyMatrix = getMatrix(file);
        int[] countryColors = new int[adjacencyMatrix.length];
        Arrays.fill(countryColors, -1);
        solve(adjacencyMatrix, countryColors);
        printArray(countryColors);
    }

    public static void solve(boolean[][] adjacencyMatrix, int[] countryColors) {

        int number = countryColors.length;
        int country = 0;
        boolean[] colors = new boolean[4];
        while (country < number) {
            Arrays.fill(colors, true);

            checkNeighbors(adjacencyMatrix, colors, countryColors, number, country);
            
            int freeColor = freeColor(colors, 5); //5 will never appear in colors[]

            if (freeColor == -1) {
                while (freeColor == -1) {
                    country--;
                    checkNeighbors(adjacencyMatrix, colors, countryColors, number, country);
                    freeColor = freeColor(colors, countryColors[country]);
                }
            }
            countryColors[country] = freeColor;
            country++;
        }
    }
    
    public static void checkNeighbors
            (boolean[][] adjacencyMatrix, boolean[] colors, int[] countryColors, int number, int country) {

        for (int i = 0; i < number; i++) {
            if (adjacencyMatrix[country][i] && i != country && countryColors[i] != -1)
                colors[countryColors[i]] = false;
        }
    }

    public static int freeColor(boolean[] colors, int exception) {
        
        for (int i = 0; i < 4; i++)
            if (colors[i] && i != exception)
                return i;
        
        return -1;
    }

    public static boolean[][] getMatrix(File file) throws FileNotFoundException {

        Scanner in = new Scanner(file);
        String[] digits;
        boolean[][] matrix = new boolean[0][0];
        int k = 0;
        String s = in.nextLine();
        digits = s.split(" ");
        matrix = new boolean[digits.length][digits.length];

        while (in.hasNextLine()) {
            digits = s.split(" ");
            for (int i = 0; i < digits.length; i++)
                matrix[k][i] = digits[i].equals("1");
            k++;
            s = in.nextLine();
        }

        digits = s.split(" ");
        for (int i = 0; i < digits.length; i++)
            matrix[k][i] = digits[i].equals("1");

        return matrix;
    }

    public static void printArray(int[] countryColors) {

        String[] colors = {"red", "blue", "green", "yellow"};

        for (int i = 0; i < countryColors.length; i++)
            System.out.println("Country " + (i + 1) + ": " + colors[countryColors[i]]);
    }
}
