package reflection2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Formatter {

    private Converter converter;

    public static final String head = "<html>\n" +
            " <head>\n" +
            "  <meta charset=\"utf-8\">\n" +
            "  <title>Тег P</title>\n" +
            " </head>\n" +
            " <body>\n";

    public static final String foot = "</body>\n" +
            "</html>";

    public Formatter(Converter converter) {

        this.converter = converter;
    }

    public void format(BufferedReader br, PrintWriter pw) throws IOException {

        String s = br.readLine();
        if (converter.getClass().equals(new LiConverter()))
        while (s != null) {
            pw.println(converter.convert(s));
            pw.flush();
            s = br.readLine();
        }
    }

    public void open(PrintWriter pw) {

        pw.println(head);
        pw.flush();
    }

    public void addTag(PrintWriter pw) {

        pw.println("<ul>");
    }

    public void close(PrintWriter pw) {

        pw.println(foot);
        pw.flush();
    }
}
