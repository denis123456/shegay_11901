package sem1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CircleList {

    Node head, end;

    public CircleList(String filename) throws FileNotFoundException {

        File file = new File(filename);
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            String [] a = s.split(" ");
            Participant p = new Participant(a[0], a[1]);
            this.insert(p);
        }
    }

    public CircleList() {

        head = null;
        end = null;
    }

    public void show() {

        Node current = head;
        boolean check = false;

        if (head != null)
            while (!check) {
                System.out.println(current.getValue());
                current = current.next;
                if (current == head)
                    check = true;
            }
        else
            System.out.println("List is empty");
    }

    public void insert(Participant p) {

        Node current = new Node(p);

        if (head == null) {
            head = current;
            end = head;
            head.next = end;
            end.next = head;
        } else {
            end.next = current;
            end = current;
            end.next = head;
        }
    }

    public void delete(String name) {

        boolean check = false;
        Node current = head;

        while (!check && current.next.getValue().getName() != name) {
            current = current.next;
            if (current == head)
                check = true;
        }

        if (!check)
            current.next = current.next.next;
    }


    public void sort(String name) {

        Participant p = search(name);
        CircleList[] list = gender();
        if (p.getGender() == "male");
            maleConcat(list[0], list[1]);
        if (p.getGender() == "female")
            femaleConcat(list[0], list[1]);
    }

    public Participant last(int k) {

        Node current = head;
        Participant p = new Participant();
        int i = 1;
        while (current.next != null) {
            if (i % k == k - 1)
                current.next = current.next.next;
            else
                p = current.getValue();
            current = current.next;
            i++;
        }

        return p;
    }

    public CircleList[] gender() {

        CircleList[] list = new CircleList[2];
        list[0] = new CircleList();
        list[1] = new CircleList();
        boolean check = false;
        Node current = head;

        while (!check) {
            if (current.getValue().getGender() == "male")
                list[0].insert(current.getValue());
            if (current.getValue().getGender() == "female")
                list[1].insert(current.getValue());
            current = current.next;
            if (current == head)
                check = true;
        }

        return list;
    }

    public void maleConcat(CircleList list1, CircleList list2) {

        head = list1.head;
        list1.end.next = list2.head;
        end = list2.end;
        end.next = head;
    }

    public void femaleConcat(CircleList list1, CircleList list2) {

        head = list2.head;
        list2.end.next = list1.head;
        end = list1.end;
        end.next = head;
    }

    public Participant search(String name) {

        boolean check = false;
        Node current = head;

        while(!check && current.getValue().getName() != name) {
            current = current.next;
            if (current == head)
                check = true;
        }

        if (!check)
            return current.getValue();
        else
            return null;
    }
}
