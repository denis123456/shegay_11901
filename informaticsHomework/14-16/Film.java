package filmTask;

public class Film {

    private String name;
    private int id;
    private int year;
    private int producerId;

    public Film(String name, int id, int year, int producerId) {

        this.name = name;
        this.id = id;
        this.producerId = producerId;
        this.year = year;
    }

    public String getName() {

        return name;
    }

    public int getId() {

        return id;
    }

    public int getYear() {

        return year;
    }

    public int getProducerId() {

        return producerId;
    }

    public String toString() {

        return name + ", " + year;
    }

}
