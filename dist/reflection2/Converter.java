package reflection2;

public abstract class Converter {

    public abstract String convert(String s);
}
